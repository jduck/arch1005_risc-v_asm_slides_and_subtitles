on sansExt(theFileName)
  do shell script "file=" & theFileName & ";" & "echo ${file%.*}"
end sansExt

on getExt(theFileName)
  do shell script "file=" & theFileName & ";" & "echo ${file##*.}"
end getExt

on run argv
  set keynote_path to (item 1 of argv)
  set out_path to (item 2 of argv)
  set extension to getExt(out_path)
  set basename to sansExt(out_path)

  tell application "Keynote"
    set keynote_file to open (keynote_path as POSIX file)
    if extension is equal to "pdf" then
      export keynote_file to (out_path as POSIX file) as PDF with properties {all stages:true, PDF image quality:best}
    else if extension is equal to "pptx" then
      export keynote_file to (out_path as POSIX file) as Microsoft PowerPoint
    else
      do shell script "echo Output format " & extension & " not supported."
    end
    close keynote_file saving no
  end tell
end run
