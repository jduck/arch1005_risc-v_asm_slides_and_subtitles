1
00:00:00,320 --> 00:00:06,399
so the takeaway from example two MO is

2
00:00:02,960 --> 00:00:08,599
that we had the MULHU being required to

3
00:00:06,399 --> 00:00:10,840
get the high exling bits of a

4
00:00:08,599 --> 00:00:12,599
multiplication unlike the M we saw

5
00:00:10,840 --> 00:00:15,200
before which keeps only the low Xing

6
00:00:12,599 --> 00:00:17,400
bits and it seems to be that GCC

7
00:00:15,200 --> 00:00:20,039
recognized that these constants are not

8
00:00:17,400 --> 00:00:22,400
going to be ever possibly treated as

9
00:00:20,039 --> 00:00:24,640
like signed 64-bit values because they

10
00:00:22,400 --> 00:00:27,400
don't have their upper sign bit set and

11
00:00:24,640 --> 00:00:29,679
then it chose to use the M hu for unsign

12
00:00:27,400 --> 00:00:32,239
times unsign multiplication as part of

13
00:00:29,679 --> 00:00:34,239
the larger 128bit multiplication that

14
00:00:32,239 --> 00:00:36,960
was going on and obviously because we

15
00:00:34,239 --> 00:00:39,280
did the 128bit multiplication that led

16
00:00:36,960 --> 00:00:41,079
to much more complicated code that

17
00:00:39,280 --> 00:00:43,079
ultimately had to do you know three

18
00:00:41,079 --> 00:00:44,800
overall multiplications and sometimes

19
00:00:43,079 --> 00:00:47,800
you know two assembly instructions per

20
00:00:44,800 --> 00:00:50,800
multiplication in order to calculate the

21
00:00:47,800 --> 00:00:53,199
bottom 128 bits of the result that goes

22
00:00:50,800 --> 00:00:54,879
back into a and is subsequently returned

23
00:00:53,199 --> 00:00:58,120
also I guess another thing you should

24
00:00:54,879 --> 00:01:01,000
see is that you did see a0 and a1 being

25
00:00:58,120 --> 00:01:03,399
set to the uh most significant and least

26
00:01:01,000 --> 00:01:05,400
significant bits respectively of the

27
00:01:03,399 --> 00:01:07,520
return value so you can return an INT

28
00:01:05,400 --> 00:01:10,080
128 even though it's bigger than the

29
00:01:07,520 --> 00:01:13,040
XLEN register size and the calling

30
00:01:10,080 --> 00:01:14,799
Convention of RISC-V is that a z and a1

31
00:01:13,040 --> 00:01:17,400
are used for return values a z is the

32
00:01:14,799 --> 00:01:19,000
low bits a1 is the high bits now if you

33
00:01:17,400 --> 00:01:21,240
weren't sure what was going on with this

34
00:01:19,000 --> 00:01:23,439
assembly I did record Another optional

35
00:01:21,240 --> 00:01:25,079
video that goes line by line in sort of

36
00:01:23,439 --> 00:01:28,320
my interpretation of what's going on and

37
00:01:25,079 --> 00:01:28,320
you can watch that next

