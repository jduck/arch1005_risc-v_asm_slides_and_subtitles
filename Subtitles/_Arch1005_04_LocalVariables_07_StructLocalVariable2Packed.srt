1
00:00:00,080 --> 00:00:05,000
now as a reminder this is what our

2
00:00:01,520 --> 00:00:07,439
structure local variable 20 no cookie

3
00:00:05,000 --> 00:00:09,599
looked like but I had made reference

4
00:00:07,439 --> 00:00:11,599
before to the fact that I didn't kind of

5
00:00:09,599 --> 00:00:15,040
like this little bit of uninitialized

6
00:00:11,599 --> 00:00:16,640
space here after a and before B I said

7
00:00:15,040 --> 00:00:18,199
like why can't we just you know squish

8
00:00:16,640 --> 00:00:19,800
it all together of course I was dealing

9
00:00:18,199 --> 00:00:21,279
with you know ins before we were talking

10
00:00:19,800 --> 00:00:23,640
about local variables and stuff like

11
00:00:21,279 --> 00:00:26,199
that but it turns out that there is this

12
00:00:23,640 --> 00:00:28,560
thing called attribute packed which you

13
00:00:26,199 --> 00:00:30,800
can apply to structs and that tells the

14
00:00:28,560 --> 00:00:33,079
compiler hey don't put any sort of

15
00:00:30,800 --> 00:00:35,840
padding like this between the interior

16
00:00:33,079 --> 00:00:37,680
members of a struct just compress it all

17
00:00:35,840 --> 00:00:40,120
together so that it's literally two

18
00:00:37,680 --> 00:00:42,760
bytes followed by8 bytes followed by 8

19
00:00:40,120 --> 00:00:45,480
bytes etc etc etc so attribute packed

20
00:00:42,760 --> 00:00:47,120
that's the GCC version it can be named

21
00:00:45,480 --> 00:00:49,199
different ways in different compilers

22
00:00:47,120 --> 00:00:51,800
but it just says structs should be

23
00:00:49,199 --> 00:00:53,440
squished as tightly as possible and you

24
00:00:51,800 --> 00:00:55,440
may or may not have ever run into this

25
00:00:53,440 --> 00:00:57,120
it kind of depends on whether you were

26
00:00:55,440 --> 00:00:58,920
trying to do you know for instance some

27
00:00:57,120 --> 00:01:01,320
particular struct that has you know very

28
00:00:58,920 --> 00:01:03,239
strict uh guidance about how it looks so

29
00:01:01,320 --> 00:01:04,839
you may or may not have ever noticed the

30
00:01:03,239 --> 00:01:06,880
fact that you can actually have these

31
00:01:04,839 --> 00:01:09,040
empty padding bites between members of a

32
00:01:06,880 --> 00:01:12,159
struct so let's go ahead and add that

33
00:01:09,040 --> 00:01:16,240
attribute packed to our myuct and

34
00:01:12,159 --> 00:01:19,320
compile it up and that yields this but

35
00:01:16,240 --> 00:01:22,240
this is a little bit ridiculous look at

36
00:01:19,320 --> 00:01:24,560
that this completely explodes the code

37
00:01:22,240 --> 00:01:26,040
size additionally it adds new assembly

38
00:01:24,560 --> 00:01:28,320
instructions that we're not ready to

39
00:01:26,040 --> 00:01:30,720
look at yet like ands for Boolean logic

40
00:01:28,320 --> 00:01:32,320
and shifts so you know what I think

41
00:01:30,720 --> 00:01:35,520
we're going to come back to this later

42
00:01:32,320 --> 00:01:35,520
in the class

