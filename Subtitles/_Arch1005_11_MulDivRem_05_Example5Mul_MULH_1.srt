1
00:00:00,199 --> 00:00:04,799
so we've just got two more multiplies to

2
00:00:02,200 --> 00:00:07,560
go and I have this variant of something

3
00:00:04,799 --> 00:00:10,880
we saw earlier we earlier saw the

4
00:00:07,560 --> 00:00:12,880
integer 128 and a hard-coded constant

5
00:00:10,880 --> 00:00:16,720
value this is the same thing but we're

6
00:00:12,880 --> 00:00:20,400
taking in input from a2l which is asky

7
00:00:16,720 --> 00:00:22,960
string to long now a long is a 32-bit

8
00:00:20,400 --> 00:00:25,439
value not a 64-bit value so when we

9
00:00:22,960 --> 00:00:28,359
compile that with size minimization we

10
00:00:25,439 --> 00:00:31,400
get this very simple code with only a

11
00:00:28,359 --> 00:00:34,160
single m h as a new assembly instruction

12
00:00:31,400 --> 00:00:37,040
and a m so what's the m h that is

13
00:00:34,160 --> 00:00:39,360
multiply keeping the high X length and

14
00:00:37,040 --> 00:00:42,840
it is intended to be used for sign times

15
00:00:39,360 --> 00:00:45,600
signed as opposed to the m hu for unsign

16
00:00:42,840 --> 00:00:49,520
times unsign that we saw before so take

17
00:00:45,600 --> 00:00:53,199
the signed rs1 times the signed rs2 and

18
00:00:49,520 --> 00:00:55,520
store the high XLEN into register

19
00:00:53,199 --> 00:00:58,280
destination and that's exactly what that

20
00:00:55,520 --> 00:01:00,640
says and M H as mentioned is supposed to

21
00:00:58,280 --> 00:01:02,519
be used when both arguments are signed

22
00:01:00,640 --> 00:01:09,280
so going back to this example from

23
00:01:02,519 --> 00:01:12,720
before hex 20 * hex 1 and 15 Zer equals

24
00:01:09,280 --> 00:01:15,360
two for the upper 64 bits and do the

25
00:01:12,720 --> 00:01:19,400
multiply to capture the lower 64 bits

26
00:01:15,360 --> 00:01:22,520
X20 * xx1 and all zeros equals Zer in

27
00:01:19,400 --> 00:01:27,280
the lower so we know that hex2 * 1 and

28
00:01:22,520 --> 00:01:29,439
15 Zer is going to be two and 16 zeros

29
00:01:27,280 --> 00:01:31,960
so it's just that simple but you want to

30
00:01:29,439 --> 00:01:34,200
step through the assembly anyways so

31
00:01:31,960 --> 00:01:36,720
that you can understand why the compiler

32
00:01:34,200 --> 00:01:39,240
could get away with doing such a small

33
00:01:36,720 --> 00:01:41,799
amount of code compared to the extremely

34
00:01:39,240 --> 00:01:43,799
large amount of code we saw before so

35
00:01:41,799 --> 00:01:46,079
I'll let you make your guesses about why

36
00:01:43,799 --> 00:01:49,719
that could be after you look at the code

37
00:01:46,079 --> 00:01:49,719
and step through the assembly

