1
00:00:00,160 --> 00:00:03,240
so what can one say about Boolean

2
00:00:02,280 --> 00:00:06,319
because

3
00:00:03,240 --> 00:00:08,639
202 well we can say that again the

4
00:00:06,319 --> 00:00:10,920
compiler will generally create one toone

5
00:00:08,639 --> 00:00:13,320
mappings between Boolean operations like

6
00:00:10,920 --> 00:00:15,480
or but sometimes you have to go out of

7
00:00:13,320 --> 00:00:18,039
your way to you know trick it into

8
00:00:15,480 --> 00:00:20,480
generating exactly the sort of rote

9
00:00:18,039 --> 00:00:23,039
compilation you want with something like

10
00:00:20,480 --> 00:00:25,760
optimization but then hiding things like

11
00:00:23,039 --> 00:00:27,880
input arguments via command line and so

12
00:00:25,760 --> 00:00:30,160
with that we found ourselves an aie

13
00:00:27,880 --> 00:00:32,160
which was explicitly here and we got

14
00:00:30,160 --> 00:00:35,160
ourselves an Andy which was not

15
00:00:32,160 --> 00:00:37,160
explicitly here but was used because of

16
00:00:35,160 --> 00:00:39,320
the sort of truncation that's going on

17
00:00:37,160 --> 00:00:41,000
here we're ultimately returning a

18
00:00:39,320 --> 00:00:43,239
character we just want to make sure that

19
00:00:41,000 --> 00:00:45,239
we only put a character value into K

20
00:00:43,239 --> 00:00:48,480
before we return it back as the return

21
00:00:45,239 --> 00:00:50,920
from main so we picked up an ay and an

22
00:00:48,480 --> 00:00:54,039
Andy and now we have a happy little

23
00:00:50,920 --> 00:00:55,440
family of Zori ory and Andy and that's

24
00:00:54,039 --> 00:00:57,800
going to do it for the Boolean

25
00:00:55,440 --> 00:00:59,440
instructions of RISC-V the only thing

26
00:00:57,800 --> 00:01:02,920
left is for some of you to go off and

27
00:00:59,440 --> 00:01:05,000
name your kids zor Orie or not Andy

28
00:01:02,920 --> 00:01:09,799
because that's too easy but zor would be

29
00:01:05,000 --> 00:01:09,799
an awesome name for a kid so do that

