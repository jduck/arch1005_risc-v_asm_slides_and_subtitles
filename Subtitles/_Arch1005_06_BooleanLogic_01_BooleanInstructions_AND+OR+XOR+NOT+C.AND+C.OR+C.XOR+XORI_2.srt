1
00:00:00,080 --> 00:00:04,080
so again you could have made a stack

2
00:00:01,400 --> 00:00:05,720
diagram I chose not to but let's see

3
00:00:04,080 --> 00:00:08,920
which instructions we've collected in

4
00:00:05,720 --> 00:00:10,120
this section we've got and we've got or

5
00:00:08,920 --> 00:00:13,280
and we've got

6
00:00:10,120 --> 00:00:16,880
exor but there's not a

7
00:00:13,280 --> 00:00:18,800
not not news is that Knot's not a real

8
00:00:16,880 --> 00:00:20,960
instruction it's a pseudo instruction

9
00:00:18,800 --> 00:00:22,880
open your eyes sheeple let's go ahead

10
00:00:20,960 --> 00:00:27,199
and blow away that pseudo instruction

11
00:00:22,880 --> 00:00:30,160
and see what we get boom and we get X or

12
00:00:27,199 --> 00:00:33,399
I instead so compressed and in instead

13
00:00:30,160 --> 00:00:37,280
of regular and X or I instead of not

14
00:00:33,399 --> 00:00:39,399
compressed or and compressed xor so not

15
00:00:37,280 --> 00:00:42,000
is not a real assembly instruction it's

16
00:00:39,399 --> 00:00:43,800
actually just an exor with negative one

17
00:00:42,000 --> 00:00:47,039
devious pseudo instruction hiding

18
00:00:43,800 --> 00:00:51,120
amongst the booleans so what is an xor i

19
00:00:47,039 --> 00:00:53,120
or zor it is exor with immediate bitwise

20
00:00:51,120 --> 00:00:55,320
exor with immediate so now instead of

21
00:00:53,120 --> 00:00:57,160
xoring two registers we're going to

22
00:00:55,320 --> 00:01:00,039
exort a register with an immediate

23
00:00:57,160 --> 00:01:02,239
source register immediate 12 bits sign

24
00:01:00,039 --> 00:01:03,879
extended and write the result into the

25
00:01:02,239 --> 00:01:08,000
destination register so it's just

26
00:01:03,879 --> 00:01:10,439
Boolean bitwise xor same as before so

27
00:01:08,000 --> 00:01:13,759
here we can see that you know we took

28
00:01:10,439 --> 00:01:16,479
this random value we exort it with all

29
00:01:13,759 --> 00:01:19,040
ones which is negative one right it's FF

30
00:01:16,479 --> 00:01:23,880
and then sign extended up it's going to

31
00:01:19,040 --> 00:01:27,119
be all ones and so one xor one is 0 0

32
00:01:23,880 --> 00:01:29,600
xor one is one and so on and so forth

33
00:01:27,119 --> 00:01:30,400
until we see tesselated toads tesselated

34
00:01:29,600 --> 00:01:33,320
toad

35
00:01:30,400 --> 00:01:33,320
tesselated

36
00:01:34,720 --> 00:01:39,920
toad so yeah X or I just use an i of

37
00:01:38,439 --> 00:01:43,280
negative one and you've got yourself a

38
00:01:39,920 --> 00:01:46,520
not instruction so the takeaways from

39
00:01:43,280 --> 00:01:48,640
this is that C Boolean operators

40
00:01:46,520 --> 00:01:51,240
basically correspond one to one to the

41
00:01:48,640 --> 00:01:53,200
assembly instructions and that means the

42
00:01:51,240 --> 00:01:55,479
hardest part of this class is over from

43
00:01:53,200 --> 00:01:58,240
here on out we're mostly just going to

44
00:01:55,479 --> 00:02:00,920
be seeing easy things where the C

45
00:01:58,240 --> 00:02:02,640
corresponds to the assembly instructions

46
00:02:00,920 --> 00:02:05,320
you've learned about the stack you've

47
00:02:02,640 --> 00:02:07,600
learned about frame pointer storage

48
00:02:05,320 --> 00:02:09,440
return address storage all the hard

49
00:02:07,600 --> 00:02:11,360
stuff is behind you and from now on

50
00:02:09,440 --> 00:02:13,360
we're just running around collecting new

51
00:02:11,360 --> 00:02:16,280
assembly instructions because we're

52
00:02:13,360 --> 00:02:20,879
completionists like that so we picked up

53
00:02:16,280 --> 00:02:23,560
X or I xor or and and and those would

54
00:02:20,879 --> 00:02:27,959
show up on this diagram right here and

55
00:02:23,560 --> 00:02:30,800
or xor and xor as well as compressed and

56
00:02:27,959 --> 00:02:33,280
compressed or and compressed X or and

57
00:02:30,800 --> 00:02:36,120
also not which is not an instruction

58
00:02:33,280 --> 00:02:38,599
It's actually an xori so yeah we just

59
00:02:36,120 --> 00:02:40,480
picked up a pretty nice chunk of Boolean

60
00:02:38,599 --> 00:02:43,200
assembly instructions although it does

61
00:02:40,480 --> 00:02:46,120
look conspicuously incomplete let's

62
00:02:43,200 --> 00:02:46,120
continue on

