1
00:00:00,040 --> 00:00:04,440
so the very trivial takeaway from if

2
00:00:02,000 --> 00:00:06,759
example 2 is that if we make something

3
00:00:04,440 --> 00:00:08,840
unsigned we're going to get an unsigned

4
00:00:06,759 --> 00:00:10,960
comparison otherwise it's exactly the

5
00:00:08,840 --> 00:00:12,719
same there's a b& there's a branch if

6
00:00:10,960 --> 00:00:14,799
greater than or equal to unsigned where

7
00:00:12,719 --> 00:00:16,840
it already knows it's not going to be

8
00:00:14,799 --> 00:00:19,039
equal and then the same thing of

9
00:00:16,840 --> 00:00:21,840
swapping the parameters so picked up

10
00:00:19,039 --> 00:00:23,640
another assembly instruction BGE working

11
00:00:21,840 --> 00:00:26,080
our way towards collecting all the

12
00:00:23,640 --> 00:00:27,800
branch instructions and here we go

13
00:00:26,080 --> 00:00:31,279
there's another one at the bottom right

14
00:00:27,800 --> 00:00:31,279
corner of our diagram

