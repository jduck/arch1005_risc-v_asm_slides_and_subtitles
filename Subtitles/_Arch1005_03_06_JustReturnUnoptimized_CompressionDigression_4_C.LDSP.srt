1
00:00:00,560 --> 00:00:05,680
one last compressed instruction to go

2
00:00:02,560 --> 00:00:10,160
the C ldsp so what is that let's check

3
00:00:05,680 --> 00:00:13,400
out this helpful diagram C ldsp up we go

4
00:00:10,160 --> 00:00:16,400
looking for an l and down here we have C

5
00:00:13,400 --> 00:00:18,800
nothing load double word using stack

6
00:00:16,400 --> 00:00:20,600
pointer so that's what we're looking for

7
00:00:18,800 --> 00:00:23,640
and what does this expand to well it

8
00:00:20,600 --> 00:00:28,679
expands to just a normal LD load double

9
00:00:23,640 --> 00:00:31,000
word s0 is the destination and 8 plus SP

10
00:00:28,679 --> 00:00:33,559
so SP plus 8 go to memory pull it out

11
00:00:31,000 --> 00:00:35,079
stick it into s0 so nothing too

12
00:00:33,559 --> 00:00:36,520
complicated there now that we've already

13
00:00:35,079 --> 00:00:40,000
seen C

14
00:00:36,520 --> 00:00:43,079
sdsp so like other loads this one is red

15
00:00:40,000 --> 00:00:44,960
from right to left so we have SP

16
00:00:43,079 --> 00:00:48,719
parentheses so we're going to memory at

17
00:00:44,960 --> 00:00:52,440
SP plus unsigned immediate 9 go to

18
00:00:48,719 --> 00:00:55,879
memory and put it into rs2 so SP plus

19
00:00:52,440 --> 00:00:57,840
zero extended this unsigned immediate 9

20
00:00:55,879 --> 00:01:01,199
and then take it from memory and put

21
00:00:57,840 --> 00:01:04,400
into rs2 this um9 is also scaled by

22
00:01:01,199 --> 00:01:06,720
eight just like with sdsp so again it's

23
00:01:04,400 --> 00:01:09,600
a 9bit value but only six bits are

24
00:01:06,720 --> 00:01:12,159
actually in the instruction stream and

25
00:01:09,600 --> 00:01:15,240
so it's going to be multiples of eight 0

26
00:01:12,159 --> 00:01:17,520
8 16 Etc and that's pretty much it that

27
00:01:15,240 --> 00:01:20,759
is it for the all of these new

28
00:01:17,520 --> 00:01:22,799
compressed instructions so like I said I

29
00:01:20,759 --> 00:01:25,079
want to cover these early in the class

30
00:01:22,799 --> 00:01:26,840
because we are at like this simplest

31
00:01:25,079 --> 00:01:28,960
phase right here right and so if even

32
00:01:26,840 --> 00:01:30,640
this simple code has this then as we get

33
00:01:28,960 --> 00:01:32,600
to more complicated stuff you'll

34
00:01:30,640 --> 00:01:35,159
actually see this stuff coming up again

35
00:01:32,600 --> 00:01:37,040
and we also talked about how uh this is

36
00:01:35,159 --> 00:01:38,640
just basically setting up the frame

37
00:01:37,040 --> 00:01:40,439
pointer and this is just tearing down

38
00:01:38,640 --> 00:01:41,799
the frame pointer so you're going to see

39
00:01:40,439 --> 00:01:43,880
it over and over again unless you

40
00:01:41,799 --> 00:01:45,640
explicitly optimize to a level that

41
00:01:43,880 --> 00:01:48,680
doesn't use frame pointers or explicitly

42
00:01:45,640 --> 00:01:51,399
say don't use frame pointers so just to

43
00:01:48,680 --> 00:01:52,960
recap like how you can read these as you

44
00:01:51,399 --> 00:01:55,240
continue to see these every time we

45
00:01:52,960 --> 00:01:57,200
invoke the chaos magic and get rid of

46
00:01:55,240 --> 00:01:59,640
the aliases you will see these things

47
00:01:57,200 --> 00:02:01,560
pop up but you can just treat them as

48
00:01:59,640 --> 00:02:03,439
okay okay compressed ADDI it's really

49
00:02:01,560 --> 00:02:06,399
just an ADI where the source and

50
00:02:03,439 --> 00:02:09,360
destination are the same SP minus 16

51
00:02:06,399 --> 00:02:12,480
back into SP sdsp can pretty much just

52
00:02:09,360 --> 00:02:16,800
ignore the SP and read it like an SD

53
00:02:12,480 --> 00:02:19,680
instruction so store s0 into SP plus 8

54
00:02:16,800 --> 00:02:23,280
in this case so just treat it like a SD

55
00:02:19,680 --> 00:02:26,239
ad I4 SPN well that's really just an ad

56
00:02:23,280 --> 00:02:28,080
ey the times 4 is just already encoded

57
00:02:26,239 --> 00:02:30,319
in here so you don't need to care about

58
00:02:28,080 --> 00:02:32,920
the time 4 just if it says it's 16 it's

59
00:02:30,319 --> 00:02:35,680
16 and the SP just means it's hardcoded

60
00:02:32,920 --> 00:02:39,760
and using the SP register so just read

61
00:02:35,680 --> 00:02:42,239
it like a ADDI so you have SP plus 16

62
00:02:39,760 --> 00:02:44,360
store it back like add those together

63
00:02:42,239 --> 00:02:46,200
and store it back into s0 compressed

64
00:02:44,360 --> 00:02:48,120
move well that's just like a normal move

65
00:02:46,200 --> 00:02:53,400
move that register value to that

66
00:02:48,120 --> 00:02:54,920
register and c. ldsp again ignore the SP

67
00:02:53,400 --> 00:02:56,360
that's just you know it has to use that

68
00:02:54,920 --> 00:02:59,080
to hardcode the sp sp because the

69
00:02:56,360 --> 00:03:01,400
compressed version of LD can't you know

70
00:02:59,080 --> 00:03:03,959
specify as SP so just ignore that and

71
00:03:01,400 --> 00:03:06,760
just read it like an LD so it's loading

72
00:03:03,959 --> 00:03:09,599
from memory SP plus 8 go to memory pull

73
00:03:06,760 --> 00:03:11,280
it out stick it in s0 and that's it so

74
00:03:09,599 --> 00:03:12,959
these are definitely all things where

75
00:03:11,280 --> 00:03:15,120
it's easier to read them once you know

76
00:03:12,959 --> 00:03:17,599
the encoding and you just you know don't

77
00:03:15,120 --> 00:03:19,319
care about particular suffixes and stuff

78
00:03:17,599 --> 00:03:20,519
like that while the suffix is trying to

79
00:03:19,319 --> 00:03:22,519
tell you something about it when it

80
00:03:20,519 --> 00:03:25,360
comes to reading it's easier to just

81
00:03:22,519 --> 00:03:28,799
kind of ignore that stuff and read sdsp

82
00:03:25,360 --> 00:03:30,760
like SD add I for SPN like add I and so

83
00:03:28,799 --> 00:03:32,519
on all right so at this point in the

84
00:03:30,760 --> 00:03:34,640
class we have picked up four

85
00:03:32,519 --> 00:03:36,879
instructions five pseudo instructions

86
00:03:34,640 --> 00:03:40,080
and six compressed instructions so what

87
00:03:36,879 --> 00:03:43,840
do we got we got compressed sdsp which

88
00:03:40,080 --> 00:03:47,159
is really just an SD compressed ldsp

89
00:03:43,840 --> 00:03:49,959
which is really just an LD compressed ad

90
00:03:47,159 --> 00:03:52,720
ey which is really just an ADDI

91
00:03:49,959 --> 00:03:55,720
compressed ADDI forn which is really

92
00:03:52,720 --> 00:03:57,840
just an ADDI compressed move which is

93
00:03:55,720 --> 00:04:00,040
really just a move but move is really

94
00:03:57,840 --> 00:04:02,079
just an ADDI but of course it's easier

95
00:04:00,040 --> 00:04:03,519
to read it as a move right if you just

96
00:04:02,079 --> 00:04:05,319
say okay move the value from that

97
00:04:03,519 --> 00:04:07,000
register to that register that's easier

98
00:04:05,319 --> 00:04:10,159
than having to think about what it would

99
00:04:07,000 --> 00:04:10,159
be as an ADDI

