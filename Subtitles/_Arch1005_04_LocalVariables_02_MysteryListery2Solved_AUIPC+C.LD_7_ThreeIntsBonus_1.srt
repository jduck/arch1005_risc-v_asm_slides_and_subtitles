1
00:00:00,399 --> 00:00:04,240
but of course I know that you the people

2
00:00:02,080 --> 00:00:06,879
love the inference game and you cry out

3
00:00:04,240 --> 00:00:08,840
more more please you know please give us

4
00:00:06,879 --> 00:00:12,080
more inference game we love it and to

5
00:00:08,840 --> 00:00:14,040
that end I've provided you a bonus round

6
00:00:12,080 --> 00:00:16,359
like what I did right there his eyeball

7
00:00:14,040 --> 00:00:19,640
still shining through all right single

8
00:00:16,359 --> 00:00:21,880
local variable we had our scalable and

9
00:00:19,640 --> 00:00:23,480
uninitialized space and we know that the

10
00:00:21,880 --> 00:00:25,640
other uninitialized space this is

11
00:00:23,480 --> 00:00:27,960
alignment padding as is this 32 bits

12
00:00:25,640 --> 00:00:29,960
here so it just always allocates hex 16

13
00:00:27,960 --> 00:00:31,840
at a time seemingly both for the frame

14
00:00:29,960 --> 00:00:34,440
point or eight allocation and four local

15
00:00:31,840 --> 00:00:35,719
variables separately and independently

16
00:00:34,440 --> 00:00:39,040
that's a reminder of a single local

17
00:00:35,719 --> 00:00:41,640
variable here's your reminder of two ins

18
00:00:39,040 --> 00:00:44,280
again uninitialized padding and the 16

19
00:00:41,640 --> 00:00:47,640
bit 16 byte allocation for the local

20
00:00:44,280 --> 00:00:50,680
variables OB Obsolete and scalable so my

21
00:00:47,640 --> 00:00:53,480
question to you is three ins not three

22
00:00:50,680 --> 00:00:56,559
long Longs three ins if you added a new

23
00:00:53,480 --> 00:00:59,079
local variable K with all A's where

24
00:00:56,559 --> 00:01:00,879
would it go on this diagram what would

25
00:00:59,079 --> 00:01:04,479
the stack look like well there's your

26
00:01:00,879 --> 00:01:06,560
code there's your compilation so I want

27
00:01:04,479 --> 00:01:08,479
you to draw a stack diagram and figure

28
00:01:06,560 --> 00:01:10,400
out what this is going to look like but

29
00:01:08,479 --> 00:01:12,720
at this point you shouldn't necessarily

30
00:01:10,400 --> 00:01:14,840
need a stack diagram so indeed I'm going

31
00:01:12,720 --> 00:01:16,960
to put a quiz question on here for those

32
00:01:14,840 --> 00:01:19,479
people who are bold in adventurous go

33
00:01:16,960 --> 00:01:21,320
ahead and just answer where is the A's

34
00:01:19,479 --> 00:01:24,159
local variable the local variable K

35
00:01:21,320 --> 00:01:25,840
going to go and if you get it wrong of

36
00:01:24,159 --> 00:01:27,759
course you can go through and generate

37
00:01:25,840 --> 00:01:31,880
your own stack diagram if you get it

38
00:01:27,759 --> 00:01:31,880
right well there's no need is there

