1
00:00:00,040 --> 00:00:03,520
so we're looking at control flow

2
00:00:01,599 --> 00:00:05,960
examples and as a reminder there's two

3
00:00:03,520 --> 00:00:07,799
forms of control flow the conditional

4
00:00:05,960 --> 00:00:10,440
which is if some condition is met which

5
00:00:07,799 --> 00:00:13,160
is the ifs switches and Loops four Loops

6
00:00:10,440 --> 00:00:15,240
while Loops do while loops and so forth

7
00:00:13,160 --> 00:00:17,160
we've already seen all of those then

8
00:00:15,240 --> 00:00:19,279
there's unconditional control flow

9
00:00:17,160 --> 00:00:21,960
always go somewhere this is things like

10
00:00:19,279 --> 00:00:24,480
function calls and returns exceptions

11
00:00:21,960 --> 00:00:27,439
interrupts and goto well we've already

12
00:00:24,480 --> 00:00:31,560
seen calls and returns in the terms of

13
00:00:27,439 --> 00:00:33,520
JW and Jer and Jr and Jer and exceptions

14
00:00:31,560 --> 00:00:35,840
and interrupts are out of scope for this

15
00:00:33,520 --> 00:00:39,280
class so that just leaves us friendly

16
00:00:35,840 --> 00:00:42,600
little goto let's see how goto from C

17
00:00:39,280 --> 00:00:44,719
manifests itself in some assembly so

18
00:00:42,600 --> 00:00:46,760
here we have SIMPLE main and the

19
00:00:44,719 --> 00:00:49,160
immediate thing it does is it says go to

20
00:00:46,760 --> 00:00:51,199
my label so that means the statement

21
00:00:49,160 --> 00:00:54,160
printf skipped is going to be skipped

22
00:00:51,199 --> 00:00:56,960
and never ever executed then at my label

23
00:00:54,160 --> 00:00:59,199
it printfs go to for the win and returns

24
00:00:56,960 --> 00:01:01,519
boldface now we're going to compile this

25
00:00:59,199 --> 00:01:04,159
at optimization Z and see whether the

26
00:01:01,519 --> 00:01:07,479
compiler chooses to still include this

27
00:01:04,159 --> 00:01:09,920
and just skip over it so here is the

28
00:01:07,479 --> 00:01:12,280
code that we get out of it and we see

29
00:01:09,920 --> 00:01:14,600
that there seems to only be one instance

30
00:01:12,280 --> 00:01:18,640
of a call to put us so it did not

31
00:01:14,600 --> 00:01:21,799
include the code to print F skipped it

32
00:01:18,640 --> 00:01:24,000
just included a noop instead and if we

33
00:01:21,799 --> 00:01:26,600
recall from earlier where we had stray

34
00:01:24,000 --> 00:01:29,400
noops the compiler engineer mentioned

35
00:01:26,600 --> 00:01:32,399
that noops at optimization level zero

36
00:01:29,400 --> 00:01:35,000
are some times for things like including

37
00:01:32,399 --> 00:01:37,040
a line number for if you want you know

38
00:01:35,000 --> 00:01:38,880
if there's going to be some some code

39
00:01:37,040 --> 00:01:40,759
that leads to not actually emitting any

40
00:01:38,880 --> 00:01:42,320
code but they want to stick something in

41
00:01:40,759 --> 00:01:43,840
the assembly so that they can for

42
00:01:42,320 --> 00:01:45,799
instance put a line number into the

43
00:01:43,840 --> 00:01:47,600
debug symbols and stuff like that so

44
00:01:45,799 --> 00:01:49,719
that may be what's going on here they

45
00:01:47,600 --> 00:01:51,399
recognize that this is entirely skipped

46
00:01:49,719 --> 00:01:54,119
they just stick in a know up and then

47
00:01:51,399 --> 00:01:57,200
they move on to the calling of this

48
00:01:54,119 --> 00:01:58,840
printf okay well goto was pretty simple

49
00:01:57,200 --> 00:02:00,560
here we could make more complicated

50
00:01:58,840 --> 00:02:03,320
examples or you could make more

51
00:02:00,560 --> 00:02:05,280
complicated examples but the basics is

52
00:02:03,320 --> 00:02:08,920
that it's just going to lead to

53
00:02:05,280 --> 00:02:08,920
unconditional control flow

