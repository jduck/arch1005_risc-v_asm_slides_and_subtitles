1
00:00:00,599 --> 00:00:04,240
so at this point we've got a couple of

2
00:00:02,240 --> 00:00:06,520
things on our mystery list that we'd

3
00:00:04,240 --> 00:00:08,880
like to figure out why they're occurring

4
00:00:06,520 --> 00:00:11,360
first why GCC is over allocating space

5
00:00:08,880 --> 00:00:13,040
when it's only saving a frame pointer

6
00:00:11,360 --> 00:00:15,320
for instance in the case of our just

7
00:00:13,040 --> 00:00:17,840
returning function and then for our

8
00:00:15,320 --> 00:00:20,279
single local variable why does it over

9
00:00:17,840 --> 00:00:22,039
allocate space for that as well so to

10
00:00:20,279 --> 00:00:24,400
figure this out I'm going to invite you

11
00:00:22,039 --> 00:00:27,560
to play something I like to call the

12
00:00:24,400 --> 00:00:29,439
inference game so the objective is to

13
00:00:27,560 --> 00:00:31,960
figure out the answer to those questions

14
00:00:29,439 --> 00:00:34,079
are approach in the inference game is to

15
00:00:31,960 --> 00:00:37,440
feed multiple program inputs to the

16
00:00:34,079 --> 00:00:40,879
compiler observe the stack usage and

17
00:00:37,440 --> 00:00:43,640
infer why it's doing whatever it's doing

18
00:00:40,879 --> 00:00:46,800
so we had single local variable with a

19
00:00:43,640 --> 00:00:50,840
single inti of scalable now let's go to

20
00:00:46,800 --> 00:00:55,239
2 in. C2 local 32-bit variables scalable

21
00:00:50,840 --> 00:00:57,239
and obsolete compile it get this code

22
00:00:55,239 --> 00:00:59,079
and no new assembly instructions so

23
00:00:57,239 --> 00:01:00,840
that's good you can just go ahead and

24
00:00:59,079 --> 00:01:03,800
look at this and figure out what's going

25
00:01:00,840 --> 00:01:05,720
on so the stack at the start of that

26
00:01:03,800 --> 00:01:07,240
code looks something like this your

27
00:01:05,720 --> 00:01:08,520
frame pointer is going to be pointing

28
00:01:07,240 --> 00:01:10,280
here your stack pointer is going to be

29
00:01:08,520 --> 00:01:12,360
pointing there again we don't care

30
00:01:10,280 --> 00:01:15,920
what's already on the stack we just care

31
00:01:12,360 --> 00:01:18,880
what do 2 ins optimization level zero

32
00:01:15,920 --> 00:01:20,840
Place onto the stack so all right stop

33
00:01:18,880 --> 00:01:23,560
and step through the assembly draw

34
00:01:20,840 --> 00:01:25,560
yourself a stack diagram and start to

35
00:01:23,560 --> 00:01:27,240
infer the differences between your

36
00:01:25,560 --> 00:01:33,040
previous diagram for single local

37
00:01:27,240 --> 00:01:33,040
variable and your new diagram for 2 in

