1
00:00:00,080 --> 00:00:05,200
in our shift example three opaque I have

2
00:00:03,000 --> 00:00:07,720
intentionally made the values to shift

3
00:00:05,200 --> 00:00:09,280
by opaque to the compiler so it can't

4
00:00:07,720 --> 00:00:10,759
know at compile time how much we're

5
00:00:09,280 --> 00:00:12,360
going to shift left how much we're going

6
00:00:10,759 --> 00:00:14,679
to shift right and how much we're going

7
00:00:12,360 --> 00:00:16,960
to shift right again you can see that a

8
00:00:14,679 --> 00:00:19,160
is an unsigned long and B and C are

9
00:00:16,960 --> 00:00:21,279
assigned Longs so basically we're going

10
00:00:19,160 --> 00:00:24,080
to have everything in here you've seen

11
00:00:21,279 --> 00:00:26,760
the core shift types already shift left

12
00:00:24,080 --> 00:00:29,720
logical shift right logical and shift

13
00:00:26,760 --> 00:00:31,240
right arithmetic so based on the types

14
00:00:29,720 --> 00:00:32,960
whether it's signed or unsigned we're

15
00:00:31,240 --> 00:00:34,520
going to see the same thing but no

16
00:00:32,960 --> 00:00:38,120
longer are we going to see silly and

17
00:00:34,520 --> 00:00:41,320
sirly we're going to see sill sir and

18
00:00:38,120 --> 00:00:43,920
SRA starting with s we've got a shift

19
00:00:41,320 --> 00:00:46,239
left logical by register now it's a

20
00:00:43,920 --> 00:00:49,320
register instead of an immediate so

21
00:00:46,239 --> 00:00:52,079
we're going to take the rs1 we're going

22
00:00:49,320 --> 00:00:54,760
to shift it left by the number of bits

23
00:00:52,079 --> 00:00:56,640
specified as a value inside of this

24
00:00:54,760 --> 00:00:59,640
register and when we're dealing with

25
00:00:56,640 --> 00:01:00,440
rv32 code it's going to be shifting by 5

26
00:00:59,640 --> 00:01:03,280
bits

27
00:01:00,440 --> 00:01:07,000
and RV 64 code it's going to shift by 6

28
00:01:03,280 --> 00:01:08,799
bits but hold on a minute we can see

29
00:01:07,000 --> 00:01:10,479
that this is only a single assembly

30
00:01:08,799 --> 00:01:14,000
instruction it's not a double like we

31
00:01:10,479 --> 00:01:16,360
saw with silly so it's only an RV32I

32
00:01:14,000 --> 00:01:18,640
and I'm claiming that this is rv32 I and

33
00:01:16,360 --> 00:01:20,680
it'll behave one Way shifting by five

34
00:01:18,640 --> 00:01:23,880
bits in 32-bit code and a different way

35
00:01:20,680 --> 00:01:25,320
shifting by six bits in 64-bit code I

36
00:01:23,880 --> 00:01:26,720
seem to have just said that that

37
00:01:25,320 --> 00:01:29,040
required two different assembly

38
00:01:26,720 --> 00:01:31,600
instructions in the context of silly and

39
00:01:29,040 --> 00:01:33,720
sirly and those shifting by immediates

40
00:01:31,600 --> 00:01:37,320
so let's take a quick digression to see

41
00:01:33,720 --> 00:01:39,720
why sill is not a double set of two

42
00:01:37,320 --> 00:01:41,759
instructions the way silly was so just

43
00:01:39,720 --> 00:01:44,000
to site where I'm getting this point

44
00:01:41,759 --> 00:01:46,799
about the things being moved by five

45
00:01:44,000 --> 00:01:49,640
bits and six bits respectively here are

46
00:01:46,799 --> 00:01:52,759
the pages for the 32-bit description of

47
00:01:49,640 --> 00:01:54,520
these shifts and the 64-bit description

48
00:01:52,759 --> 00:01:56,039
now let's look at the actual encoding

49
00:01:54,520 --> 00:01:58,280
again I don't really want to get into

50
00:01:56,039 --> 00:01:59,719
encoding early in the class but

51
00:01:58,280 --> 00:02:01,759
sometimes you just got to to really

52
00:01:59,719 --> 00:02:04,759
understand understand what's going on so

53
00:02:01,759 --> 00:02:06,719
here are the encodings for our 32-bit

54
00:02:04,759 --> 00:02:08,759
instructions and we've got silly and

55
00:02:06,719 --> 00:02:11,879
sirly here and down here are the

56
00:02:08,759 --> 00:02:14,480
encodings for the 64-bit instructions

57
00:02:11,879 --> 00:02:17,720
with Sil and Surly here so I would

58
00:02:14,480 --> 00:02:20,440
direct your attention to the shed field

59
00:02:17,720 --> 00:02:24,599
here the shift amount so the shift

60
00:02:20,440 --> 00:02:30,840
amount field for a surly in 32bit is

61
00:02:24,599 --> 00:02:33,400
encoded in only five bits 20 21 22 23 24

62
00:02:30,840 --> 00:02:35,920
5 bits directing your attention down to

63
00:02:33,400 --> 00:02:39,120
the 64-bit version we can see that it's

64
00:02:35,920 --> 00:02:42,040
a little bit wider and includes bit 25

65
00:02:39,120 --> 00:02:44,599
so six bits is used and reserved

66
00:02:42,040 --> 00:02:47,599
specifically for the shift amount in the

67
00:02:44,599 --> 00:02:49,640
64-bit and this is the core reason why

68
00:02:47,599 --> 00:02:51,840
we have two different instructions one

69
00:02:49,640 --> 00:02:54,480
of them is encoded differently from the

70
00:02:51,840 --> 00:02:56,159
other so based on that logic if we go

71
00:02:54,480 --> 00:02:57,360
ahead and look at the encoding for the

72
00:02:56,159 --> 00:03:00,680
ones we're going to learn about right

73
00:02:57,360 --> 00:03:04,000
now the sill the sirl the s and we check

74
00:03:00,680 --> 00:03:07,120
the rv32 I section and the RV64I

75
00:03:04,000 --> 00:03:09,200
section if you look across the line here

76
00:03:07,120 --> 00:03:12,400
you will actually see that these things

77
00:03:09,200 --> 00:03:14,519
are encoded exactly the same so this is

78
00:03:12,400 --> 00:03:17,040
the basis of why I'm saying silly Surly

79
00:03:14,519 --> 00:03:19,720
and sray are two different instructions

80
00:03:17,040 --> 00:03:21,200
in 32-bit versus 64-bit whereas all the

81
00:03:19,720 --> 00:03:22,799
other ones all the other shifts we're

82
00:03:21,200 --> 00:03:25,040
going to learn about are the same

83
00:03:22,799 --> 00:03:28,599
instruction en coding so back to the

84
00:03:25,040 --> 00:03:30,799
point of it sill is shift left logical

85
00:03:28,599 --> 00:03:33,239
by register so again we've got a

86
00:03:30,799 --> 00:03:36,560
register and that is going to either be

87
00:03:33,239 --> 00:03:38,879
interpreted as 5 bits to shift a 32-bit

88
00:03:36,560 --> 00:03:41,280
register or 6 bits to shift a 64-bit

89
00:03:38,879 --> 00:03:43,360
register it is a logical shift we're

90
00:03:41,280 --> 00:03:45,519
looking at once again so you're shifting

91
00:03:43,360 --> 00:03:47,319
and you're filling in with zeros it's a

92
00:03:45,519 --> 00:03:49,599
left shift which means you're filling in

93
00:03:47,319 --> 00:03:52,280
the least significant bits As you move

94
00:03:49,599 --> 00:03:54,439
from right to left looking at a concrete

95
00:03:52,280 --> 00:03:59,120
example again let's assume this is RV

96
00:03:54,439 --> 00:04:01,519
64i so we've got s a4 a5 A6 let's say

97
00:03:59,120 --> 00:04:04,040
that a six is holding this value we look

98
00:04:01,519 --> 00:04:08,560
at the bottom six bits because it's RV

99
00:04:04,040 --> 00:04:10,439
64i and the bottom six bits are a C a 12

100
00:04:08,560 --> 00:04:11,560
value and that's of course because I'm

101
00:04:10,439 --> 00:04:14,120
lazy and don't want to change my

102
00:04:11,560 --> 00:04:17,799
animations where I showed shifting by 12

103
00:04:14,120 --> 00:04:20,359
so a5 is stab aabad dudes and shift left

104
00:04:17,799 --> 00:04:22,440
logical by 12 means that you get

105
00:04:20,359 --> 00:04:24,919
everything shifted over by 12 and the

106
00:04:22,440 --> 00:04:28,000
bottom 12 bits filled in with zeros and

107
00:04:24,919 --> 00:04:29,639
you lose the top 12 bits so it's exactly

108
00:04:28,000 --> 00:04:31,840
what you already learned about with

109
00:04:29,639 --> 00:04:34,199
silly it's just instead of an i you're

110
00:04:31,840 --> 00:04:36,520
using a register to find out how many

111
00:04:34,199 --> 00:04:39,039
bits to shift by and that's what I'm

112
00:04:36,520 --> 00:04:41,360
saying here on this Slide the sill is

113
00:04:39,039 --> 00:04:43,199
just the same thing as silly but user

114
00:04:41,360 --> 00:04:46,479
register for an nend bit shift instead

115
00:04:43,199 --> 00:04:49,880
of an immediate next instruction surl

116
00:04:46,479 --> 00:04:51,960
shift right logical by register and no

117
00:04:49,880 --> 00:04:53,240
big surprises here we've got a register

118
00:04:51,960 --> 00:04:55,520
it's going to be used as the shift

119
00:04:53,240 --> 00:04:57,759
amount it is a shift logical which means

120
00:04:55,520 --> 00:04:59,560
you're always filling in bits with zeros

121
00:04:57,759 --> 00:05:00,919
and it is a shift right which means

122
00:04:59,560 --> 00:05:03,120
means you're filling in the most

123
00:05:00,919 --> 00:05:06,479
significant bits now as you shift from

124
00:05:03,120 --> 00:05:09,160
left to right concretely let's assume A6

125
00:05:06,479 --> 00:05:11,520
once again holds 12 and a5 once again

126
00:05:09,160 --> 00:05:14,520
holds staba daa dudes we are going to

127
00:05:11,520 --> 00:05:16,919
shift right logical by 12 which means we

128
00:05:14,520 --> 00:05:19,319
shift over and it's always going to be

129
00:05:16,919 --> 00:05:22,520
zeros here and the other bits are going

130
00:05:19,319 --> 00:05:25,800
to be moved down by 12 and just like s

131
00:05:22,520 --> 00:05:28,120
surl is like a surly but using an

132
00:05:25,800 --> 00:05:30,319
register instead of an immediate okay

133
00:05:28,120 --> 00:05:33,080
final thing shift right eror metic by

134
00:05:30,319 --> 00:05:35,199
register exact same logic so register

135
00:05:33,080 --> 00:05:37,639
source 2 is the amount to shift by

136
00:05:35,199 --> 00:05:41,319
whether it's five bits for 32 or six

137
00:05:37,639 --> 00:05:43,120
bits for R for RV64I and it's an

138
00:05:41,319 --> 00:05:45,360
arithmetic shift now this is the one

139
00:05:43,120 --> 00:05:46,840
that's tricky it's going to shift the

140
00:05:45,360 --> 00:05:48,600
bits but then whatever the most

141
00:05:46,840 --> 00:05:52,880
significant bit is that's what's going

142
00:05:48,600 --> 00:05:56,319
to be filled in so assume RV64I A6 once

143
00:05:52,880 --> 00:05:58,600
again holds 12 and now we have Dooby

144
00:05:56,319 --> 00:06:01,800
dooby-doo and the most significant bit

145
00:05:58,600 --> 00:06:05,039
of that is one so if we take this and we

146
00:06:01,800 --> 00:06:07,520
shift it down to the right by 12 then we

147
00:06:05,039 --> 00:06:09,720
get all ones in the most significant bit

148
00:06:07,520 --> 00:06:11,919
everything else shifted Bound by 12 so

149
00:06:09,720 --> 00:06:13,800
shift it by 12 and spread the most

150
00:06:11,919 --> 00:06:16,240
significant bit to the most significant

151
00:06:13,800 --> 00:06:19,319
12 bits because we just did a 12-bit

152
00:06:16,240 --> 00:06:22,039
shift all right SRA is like Sarai or

153
00:06:19,319 --> 00:06:23,840
sray it's just not using an immediate

154
00:06:22,039 --> 00:06:26,360
it's shifting based on what the register

155
00:06:23,840 --> 00:06:27,840
tells it cool well you know what you're

156
00:06:26,360 --> 00:06:29,800
going to have to do you've got to stop

157
00:06:27,840 --> 00:06:31,360
step through the assembly and check

158
00:06:29,800 --> 00:06:33,720
understanding again practice makes

159
00:06:31,360 --> 00:06:36,880
perfect don't skip a section you got to

160
00:06:33,720 --> 00:06:36,880
step through the assembly

