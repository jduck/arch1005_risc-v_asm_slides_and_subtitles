1
00:00:00,080 --> 00:00:05,640
in shift example six moldiv there are

2
00:00:03,000 --> 00:00:08,679
suspiciously no shifts to be seen just a

3
00:00:05,640 --> 00:00:11,160
multiply and a couple of divides so

4
00:00:08,679 --> 00:00:13,839
what's going on here well when we

5
00:00:11,160 --> 00:00:15,679
compile this code we do not see any new

6
00:00:13,839 --> 00:00:18,199
assembly instruction certainly nothing

7
00:00:15,679 --> 00:00:19,680
for multiply or divide so where are the

8
00:00:18,199 --> 00:00:21,920
multiply and divide

9
00:00:19,680 --> 00:00:24,000
instructions so you may already know

10
00:00:21,920 --> 00:00:26,279
this but for those who don't I have to

11
00:00:24,000 --> 00:00:29,119
share with you the secret of the shifts

12
00:00:26,279 --> 00:00:32,360
the fact that shifting left by n Bits is

13
00:00:29,119 --> 00:00:34,160
the same as m multiplying by 2 to the N

14
00:00:32,360 --> 00:00:36,239
so if we have the number one and we

15
00:00:34,160 --> 00:00:38,879
multiply it by 8 we of course expect

16
00:00:36,239 --> 00:00:42,039
eight also if we have the number one and

17
00:00:38,879 --> 00:00:44,680
we shift it left by three bits which is

18
00:00:42,039 --> 00:00:47,039
2 to the 3 which is 8 then we should

19
00:00:44,680 --> 00:00:51,360
also get an eight so here's our number

20
00:00:47,039 --> 00:00:53,960
one shifting left by three bits and here

21
00:00:51,360 --> 00:00:57,320
we have the number eight right so we

22
00:00:53,960 --> 00:00:59,399
started with one and shift one 2 three

23
00:00:57,320 --> 00:01:01,600
bits to the left and that is indeed the

24
00:00:59,399 --> 00:01:03,800
number eight now this also works the

25
00:01:01,600 --> 00:01:05,720
same for positive and negative numbers

26
00:01:03,800 --> 00:01:08,560
so if we had a netive - 1 and we

27
00:01:05,720 --> 00:01:12,360
multiplied it by 8 or we shifted it left

28
00:01:08,560 --> 00:01:15,000
by 2 to the 3 then we should get A8 so

29
00:01:12,360 --> 00:01:18,200
here's our ne1 that is all ones in

30
00:01:15,000 --> 00:01:20,720
binary and all fs and heximal shift it

31
00:01:18,200 --> 00:01:23,240
left by three and we're filling in the

32
00:01:20,720 --> 00:01:26,479
bottom three bits with zeros and this

33
00:01:23,240 --> 00:01:28,560
value is all fs and 8 and you can

34
00:01:26,479 --> 00:01:31,720
confirm with two's complement math that

35
00:01:28,560 --> 00:01:34,320
this is8 so again overall the point is

36
00:01:31,720 --> 00:01:37,600
left shift is the same as multiplying by

37
00:01:34,320 --> 00:01:40,439
2 the N then we have divide a right

38
00:01:37,600 --> 00:01:43,159
arithmetic shift by n Bits is the same

39
00:01:40,439 --> 00:01:45,759
as dividing by 2 to the N so let's say

40
00:01:43,159 --> 00:01:48,719
we had the value positive 128 and we

41
00:01:45,759 --> 00:01:51,479
divided it by 32 that should give us

42
00:01:48,719 --> 00:01:54,520
four and we can confirm that by taking

43
00:01:51,479 --> 00:01:58,280
128 and right shifting it by 5 bits

44
00:01:54,520 --> 00:02:03,159
which is the same as 2 5 of 32 and that

45
00:01:58,280 --> 00:02:08,119
should be a division by 2 5 32 so 128 /

46
00:02:03,159 --> 00:02:11,239
32 should be 4 here's 128 in binary and

47
00:02:08,119 --> 00:02:14,160
heximal it's hex 80 and if we right

48
00:02:11,239 --> 00:02:17,200
shift that by five then we see a value

49
00:02:14,160 --> 00:02:22,000
of four so again here the one was here

50
00:02:17,200 --> 00:02:24,760
and we did a right shift by 1 2 3 4 5

51
00:02:22,000 --> 00:02:27,599
and that is indeed binary 4 an

52
00:02:24,760 --> 00:02:29,400
arithmetic Shift Works the same way for

53
00:02:27,599 --> 00:02:33,000
positive and negative numbers so if we

54
00:02:29,400 --> 00:02:36,280
started with -28 and divide by 32 or

55
00:02:33,000 --> 00:02:41,200
right shift by 5 we should get -4 so

56
00:02:36,280 --> 00:02:44,680
here is the -28 value hex all fs and 80

57
00:02:41,200 --> 00:02:46,400
and if we shift this right by five then

58
00:02:44,680 --> 00:02:48,400
all the bits are going to go down it's

59
00:02:46,400 --> 00:02:50,760
an arithmetic shift so we're going to

60
00:02:48,400 --> 00:02:53,879
fill in the top five bits with the most

61
00:02:50,760 --> 00:02:56,120
significant bit which is one and so we

62
00:02:53,879 --> 00:02:58,400
take this one and these zeros and so the

63
00:02:56,120 --> 00:03:02,400
most significant one with the zero go

64
00:02:58,400 --> 00:03:05,000
here 1 2 3 4 5 and those zeros were

65
00:03:02,400 --> 00:03:09,159
shifted down five as well and all fs and

66
00:03:05,000 --> 00:03:11,239
a C that is -4 but it is only arithmetic

67
00:03:09,159 --> 00:03:13,440
shift where we include the most

68
00:03:11,239 --> 00:03:15,159
significant bit the sign bit and we fill

69
00:03:13,440 --> 00:03:17,080
that in at the most significant bits

70
00:03:15,159 --> 00:03:19,480
only arithmetic Shift Works for positive

71
00:03:17,080 --> 00:03:21,560
and negative numbers if you do a logical

72
00:03:19,480 --> 00:03:23,440
right shift that works for positive

73
00:03:21,560 --> 00:03:26,760
numbers but it'll yield the wrong result

74
00:03:23,440 --> 00:03:29,519
for negative so again let's take our 128

75
00:03:26,760 --> 00:03:31,799
example if we shift that right by five

76
00:03:29,519 --> 00:03:33,599
but but instead we do a logical shift

77
00:03:31,799 --> 00:03:35,879
where it's always filling in zeros at

78
00:03:33,599 --> 00:03:39,080
the most significant bits then this is

79
00:03:35,879 --> 00:03:41,280
definitely not -4 this has turned into a

80
00:03:39,080 --> 00:03:43,159
positive number specifically that

81
00:03:41,280 --> 00:03:45,280
positive number and that is definitely

82
00:03:43,159 --> 00:03:47,560
not correct so again when it comes to

83
00:03:45,280 --> 00:03:49,680
divides that is where we have arithmetic

84
00:03:47,560 --> 00:03:51,799
shifts to fill in the most significant

85
00:03:49,680 --> 00:03:54,000
bit keep negative numbers negative keep

86
00:03:51,799 --> 00:03:55,959
positive numbers positive and that's

87
00:03:54,000 --> 00:03:58,519
kind of the whole point with arithmetic

88
00:03:55,959 --> 00:04:00,239
shifts so the reason the compiler would

89
00:03:58,519 --> 00:04:02,360
use something like an AR metic shift

90
00:04:00,239 --> 00:04:04,200
when it's dealing with signed variables

91
00:04:02,360 --> 00:04:05,920
is so that it can preserve the correct

92
00:04:04,200 --> 00:04:07,840
math when it's dealing with potentially

93
00:04:05,920 --> 00:04:10,000
multiplies and divides if it's supposed

94
00:04:07,840 --> 00:04:12,319
to be interpreted as a negative 2's

95
00:04:10,000 --> 00:04:14,360
complement value so basically you want

96
00:04:12,319 --> 00:04:15,920
negative values to stay negative that's

97
00:04:14,360 --> 00:04:17,639
why you fill in the most significant

98
00:04:15,920 --> 00:04:19,280
bits with the sign value because a

99
00:04:17,639 --> 00:04:21,239
negative value will always have the most

100
00:04:19,280 --> 00:04:23,479
significant bit set to one and a

101
00:04:21,239 --> 00:04:26,080
positive value will not and this is also

102
00:04:23,479 --> 00:04:28,080
why there is no shift left arithmetic

103
00:04:26,080 --> 00:04:29,840
it's not actually needed because you're

104
00:04:28,080 --> 00:04:31,960
going to get the correct result for both

105
00:04:29,840 --> 00:04:35,280
signed and unsigned numbers if you just

106
00:04:31,960 --> 00:04:37,960
use a logical shift left okay with that

107
00:04:35,280 --> 00:04:39,680
said we know already how the shifts work

108
00:04:37,960 --> 00:04:41,560
themselves but I want you to go ahead

109
00:04:39,680 --> 00:04:43,440
and walk through this code and make sure

110
00:04:41,560 --> 00:04:45,840
that you agree that it's doing the right

111
00:04:43,440 --> 00:04:48,080
thing in terms of multiplication and

112
00:04:45,840 --> 00:04:49,720
division just confirm you know step

113
00:04:48,080 --> 00:04:52,759
through the assembly step over the

114
00:04:49,720 --> 00:04:56,440
shifts and see the shifting occurring to

115
00:04:52,759 --> 00:04:56,440
see the changes and values

