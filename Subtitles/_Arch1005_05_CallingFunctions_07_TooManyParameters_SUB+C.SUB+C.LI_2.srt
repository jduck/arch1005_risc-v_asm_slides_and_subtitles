1
00:00:00,080 --> 00:00:04,400
stepping through func plus 94 the last

2
00:00:02,399 --> 00:00:07,000
store this should have been the largest

3
00:00:04,400 --> 00:00:08,800
stack diagram to date and it breaks up

4
00:00:07,000 --> 00:00:11,880
roughly like well not roughly it breaks

5
00:00:08,800 --> 00:00:14,160
up exactly like this is main's frame and

6
00:00:11,880 --> 00:00:16,440
this is func's frame so you will see

7
00:00:14,160 --> 00:00:18,160
that the arguments the two arguments

8
00:00:16,440 --> 00:00:20,519
beyond the ones that we could store in

9
00:00:18,160 --> 00:00:24,560
argument registers got stored onto the

10
00:00:20,519 --> 00:00:27,400
stack before main called into func so

11
00:00:24,560 --> 00:00:29,080
those were at the uh stack pointer

12
00:00:27,400 --> 00:00:31,400
address at the time that it was about to

13
00:00:29,080 --> 00:00:33,559
call in and that is exactly what we

14
00:00:31,400 --> 00:00:35,879
should see based on the calling

15
00:00:33,559 --> 00:00:38,280
convention ABI information that we saw

16
00:00:35,879 --> 00:00:39,760
earlier it said that the first argument

17
00:00:38,280 --> 00:00:41,520
should be placed where the stack pointer

18
00:00:39,760 --> 00:00:43,600
is with subsequent arguments at higher

19
00:00:41,520 --> 00:00:45,399
addresses so the overall takeaway that

20
00:00:43,600 --> 00:00:47,719
we have from this we see typical things

21
00:00:45,399 --> 00:00:51,399
like saved return addresses saved frame

22
00:00:47,719 --> 00:00:53,280
pointers alignment the single Z variable

23
00:00:51,399 --> 00:00:55,440
but since there's only a single 8 by

24
00:00:53,280 --> 00:00:58,640
variable there's an extra 8 bytes for

25
00:00:55,440 --> 00:01:00,840
alignment to hex uh 10 boundary but what

26
00:00:58,640 --> 00:01:02,640
was ear what was was interesting and a

27
00:01:00,840 --> 00:01:04,839
little bit weird about this is the fact

28
00:01:02,640 --> 00:01:08,080
that each of those argument registers is

29
00:01:04,839 --> 00:01:11,200
ultimately saved off to the stack now we

30
00:01:08,080 --> 00:01:13,280
could see that GCC really likes to use

31
00:01:11,200 --> 00:01:15,960
things like argument four and five as

32
00:01:13,280 --> 00:01:17,759
its temporary registers so in some sense

33
00:01:15,960 --> 00:01:19,640
if it wants to smash the argument four

34
00:01:17,759 --> 00:01:21,600
and five it makes sense to store off a

35
00:01:19,640 --> 00:01:23,360
copy of it but that doesn't seem

36
00:01:21,600 --> 00:01:25,840
strictly necessary it could have used

37
00:01:23,360 --> 00:01:28,280
things like temporary registers t0

38
00:01:25,840 --> 00:01:29,600
through T6 so once again we might infer

39
00:01:28,280 --> 00:01:32,479
that this comes down to the fact that

40
00:01:29,600 --> 00:01:35,159
we're looking at unoptimized code so

41
00:01:32,479 --> 00:01:37,640
again the arguments this was the area

42
00:01:35,159 --> 00:01:40,079
where we saw all these load immediate

43
00:01:37,640 --> 00:01:41,840
instructions storing the argument zero

44
00:01:40,079 --> 00:01:44,159
argument one argument two for each of

45
00:01:41,840 --> 00:01:45,880
our arguments and obviously I selected

46
00:01:44,159 --> 00:01:47,920
the values so that it was very clear

47
00:01:45,880 --> 00:01:50,520
that argument 0 was Zer argument one was

48
00:01:47,920 --> 00:01:52,479
11 and so forth so you could understand

49
00:01:50,520 --> 00:01:55,119
exactly that they went in in this order

50
00:01:52,479 --> 00:01:57,719
not perhaps the opposite order then we

51
00:01:55,119 --> 00:02:00,520
had those stores to the stack in main

52
00:01:57,719 --> 00:02:03,039
still of the extra arguments

53
00:02:00,520 --> 00:02:05,399
once we got into func we saw all of this

54
00:02:03,039 --> 00:02:08,119
storing of those arguments immediately

55
00:02:05,399 --> 00:02:10,800
off to the stack and we saw that when it

56
00:02:08,119 --> 00:02:14,160
actually operated on values for instance

57
00:02:10,800 --> 00:02:15,800
this uh loading from frame pointer minus

58
00:02:14,160 --> 00:02:18,239
40 well that's where it stored the

59
00:02:15,800 --> 00:02:20,200
argument zero frame pointer minus 40 so

60
00:02:18,239 --> 00:02:23,160
loading it up it really likes to use

61
00:02:20,200 --> 00:02:26,000
these a4 and a5 as its temporary things

62
00:02:23,160 --> 00:02:28,560
just reuse them over and over again and

63
00:02:26,000 --> 00:02:30,560
that's what we see here now I do smell

64
00:02:28,560 --> 00:02:33,920
deception so I do want to rewrite

65
00:02:30,560 --> 00:02:36,319
reality and see what it holds and indeed

66
00:02:33,920 --> 00:02:37,920
we have a compressed lie is being used

67
00:02:36,319 --> 00:02:40,200
here these are not just normal lies

68
00:02:37,920 --> 00:02:43,000
these are compressed lies so we said lie

69
00:02:40,200 --> 00:02:45,000
is a lie it's actually ADDI and some of

70
00:02:43,000 --> 00:02:47,800
those lies turned into ad eyes but other

71
00:02:45,000 --> 00:02:49,200
ones turned into compressed lie and

72
00:02:47,800 --> 00:02:51,920
what's the difference why would a

73
00:02:49,200 --> 00:02:53,879
compiler choose to use three compressed

74
00:02:51,920 --> 00:02:55,599
lies and add eyes for everything else

75
00:02:53,879 --> 00:02:58,640
well we'll see in a second and then we

76
00:02:55,599 --> 00:03:00,560
also picked up a compressed sub so soon

77
00:02:58,640 --> 00:03:02,360
after we just learned about sub so

78
00:03:00,560 --> 00:03:04,599
compressed sub compressed subtract

79
00:03:02,360 --> 00:03:06,200
register to register this is a two

80
00:03:04,599 --> 00:03:07,879
register form just like the compressed

81
00:03:06,200 --> 00:03:10,519
ad we learned about before and it is

82
00:03:07,879 --> 00:03:13,519
using those narrower set of possible

83
00:03:10,519 --> 00:03:16,879
registers the Rd prime and the rs2 prime

84
00:03:13,519 --> 00:03:19,879
so it does Rd prime as its source one

85
00:03:16,879 --> 00:03:22,720
minus rs2 prime as it source 2 and

86
00:03:19,879 --> 00:03:25,560
stores the result back into Rd prime so

87
00:03:22,720 --> 00:03:28,200
yes unlike the RV32I which doesn't have

88
00:03:25,560 --> 00:03:30,239
two register forms compressed is again

89
00:03:28,200 --> 00:03:32,959
just fine having the two register form

90
00:03:30,239 --> 00:03:35,799
with an implicit use of the destination

91
00:03:32,959 --> 00:03:38,080
as a source as well but one little thing

92
00:03:35,799 --> 00:03:40,879
that threw me with the CSUB when I was

93
00:03:38,080 --> 00:03:42,720
reading it is that C ad actually was

94
00:03:40,879 --> 00:03:45,879
encoded in a way that it could use the

95
00:03:42,720 --> 00:03:48,640
32 the full possible 32 registers for a

96
00:03:45,879 --> 00:03:50,799
destination for instance but C sub is

97
00:03:48,640 --> 00:03:52,840
not C sub can only use these prime

98
00:03:50,799 --> 00:03:55,640
registers that is the definition here so

99
00:03:52,840 --> 00:03:57,680
the X8 through X15 so again that's what

100
00:03:55,640 --> 00:03:59,239
I said before of if you're reading it

101
00:03:57,680 --> 00:04:01,280
you're just reading you know subtract

102
00:03:59,239 --> 00:04:03,200
some register or some register you need

103
00:04:01,280 --> 00:04:05,040
to know that it's this minus that back

104
00:04:03,200 --> 00:04:07,000
into that and you don't really care

105
00:04:05,040 --> 00:04:08,720
about you know is it some subset reading

106
00:04:07,000 --> 00:04:10,720
is easy but if you're writing you have

107
00:04:08,720 --> 00:04:12,720
to actually read the fund manual to

108
00:04:10,720 --> 00:04:15,280
figure out what restrictions apply if

109
00:04:12,720 --> 00:04:17,160
any and so here was our reminder about

110
00:04:15,280 --> 00:04:18,919
the load immediate instruction from

111
00:04:17,160 --> 00:04:20,560
before but of course the key thing you

112
00:04:18,919 --> 00:04:24,120
need to remember is that the LI is a

113
00:04:20,560 --> 00:04:27,040
lie but Li took in a 12bit immediate and

114
00:04:24,120 --> 00:04:30,000
distored it into a destination register

115
00:04:27,040 --> 00:04:32,720
in contrast the compressed Li takes say

116
00:04:30,000 --> 00:04:35,240
6bit immediate it's still sign extended

117
00:04:32,720 --> 00:04:37,400
but it is then stored into the register

118
00:04:35,240 --> 00:04:40,400
so that should give you a quick sense of

119
00:04:37,400 --> 00:04:42,840
why in the assembly we saw only three

120
00:04:40,400 --> 00:04:45,240
compressed liis for the three smaller

121
00:04:42,840 --> 00:04:48,840
values and then once we went up to a

122
00:04:45,240 --> 00:04:51,520
larger value specifically 33 then we

123
00:04:48,840 --> 00:04:55,120
could no longer use a compressed Li

124
00:04:51,520 --> 00:04:57,919
because 33 cannot be encoded in a signed

125
00:04:55,120 --> 00:05:01,240
6bit value so at that point the compiler

126
00:04:57,919 --> 00:05:04,080
went back up to the ADI for for Lis so

127
00:05:01,240 --> 00:05:06,680
with the compressed Li just as an FYI

128
00:05:04,080 --> 00:05:10,199
there is a destination not allowed to be

129
00:05:06,680 --> 00:05:13,160
x0 constraint on this so new assembly

130
00:05:10,199 --> 00:05:15,880
instructions we picked up sub and sub

131
00:05:13,160 --> 00:05:21,680
Pops in right here we also picked up a

132
00:05:15,880 --> 00:05:21,680
compressed sub and the new compressed Li

