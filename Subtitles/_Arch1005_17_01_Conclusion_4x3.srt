1
00:00:00,640 --> 00:00:05,480
you did it you finished the architecture

2
00:00:03,000 --> 00:00:07,799
1005 class are you pumped and jazzed

3
00:00:05,480 --> 00:00:10,240
because I'm pumped and jazzed you're

4
00:00:07,799 --> 00:00:11,840
basically awesome now because you know

5
00:00:10,240 --> 00:00:13,880
cool things that a lot of other people

6
00:00:11,840 --> 00:00:15,360
don't know yet of course you can help

7
00:00:13,880 --> 00:00:17,560
them learn it if you let them know about

8
00:00:15,360 --> 00:00:20,080
this class but until then just be

9
00:00:17,560 --> 00:00:21,680
confident in your awesomeness but

10
00:00:20,080 --> 00:00:24,560
although you are done with the class

11
00:00:21,680 --> 00:00:25,960
you're not really done you've just begun

12
00:00:24,560 --> 00:00:28,080
because part of my motivation for

13
00:00:25,960 --> 00:00:30,679
teaching these classes is that I want

14
00:00:28,080 --> 00:00:33,000
more peers I want more people who know

15
00:00:30,679 --> 00:00:34,600
what I know and ultimately I want people

16
00:00:33,000 --> 00:00:36,399
who are more than peers I want people

17
00:00:34,600 --> 00:00:38,800
who are better than me and go off and do

18
00:00:36,399 --> 00:00:41,520
cool things that I can check out and say

19
00:00:38,800 --> 00:00:42,960
hey that's cool so if that's you if

20
00:00:41,520 --> 00:00:45,200
you're going to do cool things in the

21
00:00:42,960 --> 00:00:46,719
future great you're off to a great start

22
00:00:45,200 --> 00:00:49,360
because it doesn't take that much to

23
00:00:46,719 --> 00:00:52,239
exceed me in Risk 5 it's relatively new

24
00:00:49,360 --> 00:00:53,840
and I'm relatively new to it so with

25
00:00:52,239 --> 00:00:56,440
just a little bit of extra effort you

26
00:00:53,840 --> 00:00:58,239
can easily exceed me perhaps you want to

27
00:00:56,440 --> 00:01:00,960
go off and learn about operating systems

28
00:00:58,239 --> 00:01:03,239
internals and make an architecture 2005

29
00:01:00,960 --> 00:01:05,400
class in any case let's see what we

30
00:01:03,239 --> 00:01:08,759
learn in the class we said that we were

31
00:01:05,400 --> 00:01:11,560
going to cover the basic RISC-V 32-bit

32
00:01:08,759 --> 00:01:15,200
and 64-bit ises and we did indeed pick

33
00:01:11,560 --> 00:01:18,280
up all 40 basic 32-bit instructions and

34
00:01:15,200 --> 00:01:20,799
the extra 15 bonus instructions from RV

35
00:01:18,280 --> 00:01:23,479
64i but above and beyond that we also

36
00:01:20,799 --> 00:01:25,640
pulled down some multiplies and divides

37
00:01:23,479 --> 00:01:28,200
pseudo instructions and compressed

38
00:01:25,640 --> 00:01:30,400
instructions so we learned a whole lot

39
00:01:28,200 --> 00:01:32,880
of instructions in this class

40
00:01:30,400 --> 00:01:35,200
but as we saw they're mostly all simple

41
00:01:32,880 --> 00:01:36,520
variants of each other permutations on

42
00:01:35,200 --> 00:01:38,200
whether an instruction takes an

43
00:01:36,520 --> 00:01:40,600
immediate as an argument versus

44
00:01:38,200 --> 00:01:42,479
registers and so they're not that hard

45
00:01:40,600 --> 00:01:44,079
there were only sort of you know nine

46
00:01:42,479 --> 00:01:46,799
major groupings of things for us to

47
00:01:44,079 --> 00:01:48,360
learn about and so we saw in the bomb

48
00:01:46,799 --> 00:01:50,320
lab if you didn't take the bomb lab

49
00:01:48,360 --> 00:01:52,000
already you definitely should don't

50
00:01:50,320 --> 00:01:54,119
short Change yourself make sure you do

51
00:01:52,000 --> 00:01:56,399
the whole bomb lab but in the bomb lab

52
00:01:54,119 --> 00:01:58,320
the statistics tell us that only 53

53
00:01:56,399 --> 00:02:00,759
assembly instructions covers every

54
00:01:58,320 --> 00:02:03,799
single instruction in in the entire big

55
00:02:00,759 --> 00:02:05,479
old binary and if you only learned 25

56
00:02:03,799 --> 00:02:08,080
you would still be able to understand

57
00:02:05,479 --> 00:02:11,640
almost all of the code and with a mere

58
00:02:08,080 --> 00:02:12,920
scant 10 you would get 2/3 of the code

59
00:02:11,640 --> 00:02:14,080
so these are the outcomes that I

60
00:02:12,920 --> 00:02:15,519
promised you at the beginning of the

61
00:02:14,080 --> 00:02:17,480
class and hopefully you see that I've

62
00:02:15,519 --> 00:02:19,160
delivered on all of them at the end of

63
00:02:17,480 --> 00:02:20,720
the class you should know the RISC-V

64
00:02:19,160 --> 00:02:23,879
general purpose registers well that's

65
00:02:20,720 --> 00:02:25,840
easy x0 through x31 and their

66
00:02:23,879 --> 00:02:28,440
alternative ABI names the things like

67
00:02:25,840 --> 00:02:30,640
stack pointer frame pointer the argument

68
00:02:28,440 --> 00:02:33,080
registers the temporary registers and so

69
00:02:30,640 --> 00:02:34,760
forth you should understand the local

70
00:02:33,080 --> 00:02:36,840
variables frame pointers and return

71
00:02:34,760 --> 00:02:38,720
addresses which we saw repeatedly

72
00:02:36,840 --> 00:02:40,640
getting put onto the stack and removed

73
00:02:38,720 --> 00:02:41,599
when functions would return should

74
00:02:40,640 --> 00:02:43,560
understand the function calling

75
00:02:41,599 --> 00:02:46,159
conventions which again is pretty simple

76
00:02:43,560 --> 00:02:48,360
just argument registers 0 through 7 for

77
00:02:46,159 --> 00:02:50,280
the first eight arguments and stick it

78
00:02:48,360 --> 00:02:53,000
on the stack after that and the return

79
00:02:50,280 --> 00:02:56,519
value is returned in the a z argument Z

80
00:02:53,000 --> 00:02:58,360
register or if you had a 128bit example

81
00:02:56,519 --> 00:03:00,480
as we saw in the multiplies you could

82
00:02:58,360 --> 00:03:04,080
also have the argument zero argument one

83
00:03:00,480 --> 00:03:06,319
to pass back 128bit value then we have

84
00:03:04,080 --> 00:03:07,760
the being comfortable writing code in C

85
00:03:06,319 --> 00:03:09,920
while I can't help you with the writing

86
00:03:07,760 --> 00:03:12,159
code andc that part you are supposed to

87
00:03:09,920 --> 00:03:14,280
have come to the class with already but

88
00:03:12,159 --> 00:03:16,760
reading the assembly and stepping

89
00:03:14,280 --> 00:03:19,080
through the consequent disassembly that

90
00:03:16,760 --> 00:03:21,319
is a result of compiling some small C

91
00:03:19,080 --> 00:03:24,040
program hopefully we did that so many

92
00:03:21,319 --> 00:03:25,680
times that it's just old hat by now also

93
00:03:24,040 --> 00:03:28,000
feeling comfortable reading the fund

94
00:03:25,680 --> 00:03:30,239
manual on your own me personally I love

95
00:03:28,000 --> 00:03:32,640
to read the fund manuals and hopefully

96
00:03:30,239 --> 00:03:34,319
you'll enjoy it as well because at the

97
00:03:32,640 --> 00:03:36,840
end of the day you do need to read the

98
00:03:34,319 --> 00:03:38,720
fun manual to know the truth you can

99
00:03:36,840 --> 00:03:40,720
Google things you can look at my

100
00:03:38,720 --> 00:03:42,720
simplification but you really need to

101
00:03:40,720 --> 00:03:44,840
read the manuals be able to write some

102
00:03:42,720 --> 00:03:47,120
of the instructions yourself just to

103
00:03:44,840 --> 00:03:49,480
confirm that what you think you read is

104
00:03:47,120 --> 00:03:51,319
what's actually going on and finally

105
00:03:49,480 --> 00:03:53,000
being able to do the bomb lab right that

106
00:03:51,319 --> 00:03:55,680
is one of the big takeaways of this

107
00:03:53,000 --> 00:03:58,120
class you only truly truly are going to

108
00:03:55,680 --> 00:03:59,959
absorb stuff when you actually use it

109
00:03:58,120 --> 00:04:02,280
and figuring out the answers to the

110
00:03:59,959 --> 00:04:04,280
binary bomb lab is the best way to

111
00:04:02,280 --> 00:04:06,720
really drill this stuff into your head

112
00:04:04,280 --> 00:04:09,680
it's foundational and it really makes it

113
00:04:06,720 --> 00:04:12,519
stick in my experience so what's next

114
00:04:09,680 --> 00:04:14,480
for you Learners of RISC-V assembly well

115
00:04:12,519 --> 00:04:16,720
that depends on what particular course

116
00:04:14,480 --> 00:04:18,479
you've charted in this game of life

117
00:04:16,720 --> 00:04:20,199
maybe you're a reverse engineer and you

118
00:04:18,479 --> 00:04:21,799
want to go off and find some targets to

119
00:04:20,199 --> 00:04:23,759
reverse engineer maybe you have a

120
00:04:21,799 --> 00:04:26,040
platform that you already know uses RISC

121
00:04:23,759 --> 00:04:28,560
5 and you want to go off and exploit it

122
00:04:26,040 --> 00:04:30,680
or maybe you just want to improve your

123
00:04:28,560 --> 00:04:32,440
software development debugging skills

124
00:04:30,680 --> 00:04:35,400
I'll provide some pointers after this

125
00:04:32,440 --> 00:04:37,240
video on the OST2 class website for

126
00:04:35,400 --> 00:04:40,280
different places people can go to

127
00:04:37,240 --> 00:04:42,120
continue their education and with that I

128
00:04:40,280 --> 00:04:45,160
appreciate you spending your time on

129
00:04:42,120 --> 00:04:48,840
this class and until next time I'll see

130
00:04:45,160 --> 00:04:48,840
you around OST2

