1
00:00:00,359 --> 00:00:06,680
once more into the breach here we go how

2
00:00:03,600 --> 00:00:09,599
about we talk about RV 32c instruction

3
00:00:06,680 --> 00:00:12,040
encoding so there's this table here that

4
00:00:09,599 --> 00:00:15,839
has all sorts of good stuff in it but we

5
00:00:12,040 --> 00:00:17,840
can see that op code is only two bits so

6
00:00:15,839 --> 00:00:19,960
that sure is not going to be doing a

7
00:00:17,840 --> 00:00:24,320
whole lot of anything but then we see

8
00:00:19,960 --> 00:00:26,320
instead like fun 4 and fun 3 and fun six

9
00:00:24,320 --> 00:00:28,160
so it looks like those minor op codes

10
00:00:26,320 --> 00:00:30,679
are going to be doing the heavy lifting

11
00:00:28,160 --> 00:00:32,239
not the primary op code and we had to

12
00:00:30,679 --> 00:00:34,320
talk about this a little bit throughout

13
00:00:32,239 --> 00:00:36,680
the class just because it came up so

14
00:00:34,320 --> 00:00:39,480
much when you see in assembly

15
00:00:36,680 --> 00:00:41,520
instructions something like rs1 tick the

16
00:00:39,480 --> 00:00:43,200
Tick is saying hey by the way this can

17
00:00:41,520 --> 00:00:44,920
only use the three bit encoding of

18
00:00:43,200 --> 00:00:47,160
instructions not the five bit encoding

19
00:00:44,920 --> 00:00:50,000
that we saw for full instructions five

20
00:00:47,160 --> 00:00:52,320
bit can get you all 32 registers three

21
00:00:50,000 --> 00:00:56,559
bits can only get you eight registers

22
00:00:52,320 --> 00:00:58,960
specifically X8 through X15 X8 though as

23
00:00:56,559 --> 00:01:01,399
we may recall is also s0 is also the

24
00:00:58,960 --> 00:01:03,960
frame pointer so if we pick a

25
00:01:01,399 --> 00:01:06,320
representative example to interpret we

26
00:01:03,960 --> 00:01:08,080
can have we can see you know C and C or

27
00:01:06,320 --> 00:01:10,360
and so forth It's a C and computes the

28
00:01:08,080 --> 00:01:14,080
bitwise and of the values in registers

29
00:01:10,360 --> 00:01:16,159
Rd prime and rs2 prime now in my actual

30
00:01:14,080 --> 00:01:19,119
description I think I stopped writing

31
00:01:16,159 --> 00:01:20,000
eventually like Rd prime rs1 prime

32
00:01:19,119 --> 00:01:21,720
because I didn't want you to think

33
00:01:20,000 --> 00:01:23,680
there's a divide or something like that

34
00:01:21,720 --> 00:01:28,479
so when I described it I probably said

35
00:01:23,680 --> 00:01:30,240
just Rd prime uh and rs2 prime going

36
00:01:28,479 --> 00:01:32,040
back into Rd prime or something like

37
00:01:30,240 --> 00:01:33,799
that again just trying to simplify it

38
00:01:32,040 --> 00:01:35,560
down make it a little bit cleaner but

39
00:01:33,799 --> 00:01:38,280
you can see where I got that from like

40
00:01:35,560 --> 00:01:39,920
why did I call the single source rs2

41
00:01:38,280 --> 00:01:42,520
well I called it that because that's the

42
00:01:39,920 --> 00:01:45,240
way it's listed in the manual so in this

43
00:01:42,520 --> 00:01:48,520
case these compressed uh binary

44
00:01:45,240 --> 00:01:50,399
operations and add and sub the W size

45
00:01:48,520 --> 00:01:53,920
things so the W size so we know this is

46
00:01:50,399 --> 00:01:56,039
a compressed 64-bit instruction these

47
00:01:53,920 --> 00:01:58,200
can only operate on the smaller register

48
00:01:56,039 --> 00:02:00,000
set then they have the op code which is

49
00:01:58,200 --> 00:02:01,479
two bits and it's C1 and we don't don't

50
00:02:00,000 --> 00:02:04,399
know what that is but then they've got

51
00:02:01,479 --> 00:02:06,719
another fun six and fun two worth of

52
00:02:04,399 --> 00:02:08,520
bits so eight bits in which to

53
00:02:06,719 --> 00:02:10,520
distinguish the and and the or and the

54
00:02:08,520 --> 00:02:12,840
exor but let's go ahead and just start

55
00:02:10,520 --> 00:02:15,519
with the C1 what is C1 where does it

56
00:02:12,840 --> 00:02:19,840
come from so if you were to go and look

57
00:02:15,519 --> 00:02:23,080
through this table 16.4 RVC op code map

58
00:02:19,840 --> 00:02:24,680
you would see a whole bunch of no C1 so

59
00:02:23,080 --> 00:02:26,760
we said in the previous op code map that

60
00:02:24,680 --> 00:02:28,720
you could you know go into this map and

61
00:02:26,760 --> 00:02:32,080
then find the thing and then reverse the

62
00:02:28,720 --> 00:02:33,680
bits from there well this case I have no

63
00:02:32,080 --> 00:02:35,920
C1 here so I don't know how that's

64
00:02:33,680 --> 00:02:37,640
supposed to be useful to me so we got to

65
00:02:35,920 --> 00:02:40,360
kind of cheat again and we got to go to

66
00:02:37,640 --> 00:02:42,560
the full description of how the

67
00:02:40,360 --> 00:02:45,120
instructions are actually encoded we

68
00:02:42,560 --> 00:02:47,680
will slice out a little piece of it for

69
00:02:45,120 --> 00:02:50,440
this exors and ores and stuff that we're

70
00:02:47,680 --> 00:02:52,440
looking at here so if we look at the

71
00:02:50,440 --> 00:02:55,000
description we can see the bottom bits

72
00:02:52,440 --> 00:02:58,480
are zero and one and this table says

73
00:02:55,000 --> 00:03:01,360
instruction bits 0 and one are in this

74
00:02:58,480 --> 00:03:04,760
column so we take the 0 and one go here

75
00:03:01,360 --> 00:03:06,440
and that corresponds to this row then

76
00:03:04,760 --> 00:03:09,239
the other part of this table the other

77
00:03:06,440 --> 00:03:12,239
axis of this table is taking instruction

78
00:03:09,239 --> 00:03:14,000
bits 13 14 and 15 recall we're

79
00:03:12,239 --> 00:03:16,159
compressed instructions they only have

80
00:03:14,000 --> 00:03:19,920
16 bits total so those are the upper

81
00:03:16,159 --> 00:03:22,640
three bits and so that is all 100s down

82
00:03:19,920 --> 00:03:25,720
here so take that go up here and go

83
00:03:22,640 --> 00:03:29,519
across and that gets you the one Z put

84
00:03:25,720 --> 00:03:31,680
them together and you get misk ALU well

85
00:03:29,519 --> 00:03:34,920
that that is not helpful at all I wanted

86
00:03:31,680 --> 00:03:36,799
something like C1 but instead I get misu

87
00:03:34,920 --> 00:03:38,840
and it kind of makes sense these are

88
00:03:36,799 --> 00:03:42,640
miscellaneous these are you know some

89
00:03:38,840 --> 00:03:44,360
binary um Boolean operations and adding

90
00:03:42,640 --> 00:03:46,560
and subtracting and stuff like that so

91
00:03:44,360 --> 00:03:48,760
they are arithmetic logical unit

92
00:03:46,560 --> 00:03:51,040
appropriate instructions but that makes

93
00:03:48,760 --> 00:03:53,319
this table less useful If instead of

94
00:03:51,040 --> 00:03:55,360
saying C1 it said M ALU then I would at

95
00:03:53,319 --> 00:03:57,560
least know that it would be encoded this

96
00:03:55,360 --> 00:04:00,879
way but let's see why they perhaps did

97
00:03:57,560 --> 00:04:02,959
that so if we go back to this we will

98
00:04:00,879 --> 00:04:05,599
see that the actual description of this

99
00:04:02,959 --> 00:04:09,120
table is instruction listings for RVC

100
00:04:05,599 --> 00:04:12,760
quadrant 1 so quadrant implies four

101
00:04:09,120 --> 00:04:15,239
things and this every single instruction

102
00:04:12,760 --> 00:04:18,320
in this table seems to have 01 as its

103
00:04:15,239 --> 00:04:20,040
bottom two bits two bits can encode four

104
00:04:18,320 --> 00:04:21,639
possible values and if you went and

105
00:04:20,040 --> 00:04:23,759
looked in the manual you would also see

106
00:04:21,639 --> 00:04:27,280
other things talking about quadrant zero

107
00:04:23,759 --> 00:04:29,360
and quadrant 2 so from this going back

108
00:04:27,280 --> 00:04:31,880
to this table we can see that yes there

109
00:04:29,360 --> 00:04:34,560
was you know zero and one and two but

110
00:04:31,880 --> 00:04:38,240
the bottom two bits always being one we

111
00:04:34,560 --> 00:04:41,160
said that's reserved for uh RV32I and

112
00:04:38,240 --> 00:04:43,080
RV64I so the compressed instructions

113
00:04:41,160 --> 00:04:47,560
can never use one one for the bottom two

114
00:04:43,080 --> 00:04:49,759
bits they must always use 0 0 01 or 1 Z

115
00:04:47,560 --> 00:04:52,080
and so for this particular diagram that

116
00:04:49,759 --> 00:04:54,880
we're showing right now it's always 01

117
00:04:52,080 --> 00:04:57,280
that corresponds to all of these sort of

118
00:04:54,880 --> 00:04:59,600
instructions so the inference I would

119
00:04:57,280 --> 00:05:04,160
want to make is that perhaps this op

120
00:04:59,600 --> 00:05:06,360
code C1 means literally 01 and we know

121
00:05:04,160 --> 00:05:08,240
that that's true for this but let's find

122
00:05:06,360 --> 00:05:12,240
out if that's true for other encodings

123
00:05:08,240 --> 00:05:14,600
so does C 0 or C1 or C2 mean quadrant or

124
00:05:12,240 --> 00:05:17,160
row0 or one or two or the compressed

125
00:05:14,600 --> 00:05:20,440
encodings for that so let's go find some

126
00:05:17,160 --> 00:05:23,400
other instruction like compressed adi4

127
00:05:20,440 --> 00:05:25,039
SPN look over here and we've got the op

128
00:05:23,400 --> 00:05:29,120
code of

129
00:05:25,039 --> 00:05:32,240
c0 and go up here and that would be this

130
00:05:29,120 --> 00:05:35,520
right here here this quadrant c0 and

131
00:05:32,240 --> 00:05:37,840
indeed we can see at I4 SPN as the very

132
00:05:35,520 --> 00:05:41,319
first example of that grab another one

133
00:05:37,840 --> 00:05:44,680
just contiguous this C silly and that is

134
00:05:41,319 --> 00:05:48,280
an OP code of C2 go up here to the two

135
00:05:44,680 --> 00:05:50,199
row and yes C silly is right there

136
00:05:48,280 --> 00:05:52,919
immediately available so it seems like

137
00:05:50,199 --> 00:05:56,919
we're good to go and the inference of up

138
00:05:52,919 --> 00:06:00,720
code C 0 is literally 0 0 up code C2 is

139
00:05:56,919 --> 00:06:04,039
literally 1 Z and up code C1 is is 01

140
00:06:00,720 --> 00:06:05,800
and with that Kylo is sated for now but

141
00:06:04,039 --> 00:06:08,599
hopefully you're not hopefully you can

142
00:06:05,800 --> 00:06:10,639
continue on and read the fun manual and

143
00:06:08,599 --> 00:06:13,400
you know just explore and understand the

144
00:06:10,639 --> 00:06:15,479
actual literal details that I've hidden

145
00:06:13,400 --> 00:06:17,280
from you throughout the class I haven't

146
00:06:15,479 --> 00:06:20,000
giving you all the possible nitty-gritty

147
00:06:17,280 --> 00:06:21,560
detail CU it's not always necessary but

148
00:06:20,000 --> 00:06:23,319
now when you really want to know what's

149
00:06:21,560 --> 00:06:25,720
going on with an instruction when you

150
00:06:23,319 --> 00:06:27,680
are really unsure go read the manual and

151
00:06:25,720 --> 00:06:29,000
if you're still unsure well in the next

152
00:06:27,680 --> 00:06:31,440
section we're going to learn how to

153
00:06:29,000 --> 00:06:33,599
write assembly ourselves and with that

154
00:06:31,440 --> 00:06:37,720
you can correlate what you see in the

155
00:06:33,599 --> 00:06:37,720
manual and what you see in the debugger

