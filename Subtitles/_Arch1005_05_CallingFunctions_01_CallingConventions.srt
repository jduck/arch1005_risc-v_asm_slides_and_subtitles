1
00:00:00,240 --> 00:00:04,400
now we've all been introduced to calling

2
00:00:02,080 --> 00:00:07,640
conventions when we use a telephone

3
00:00:04,400 --> 00:00:09,880
perhaps you answer yellow or a Hoy Hoy

4
00:00:07,640 --> 00:00:12,280
as was favored by Mr Burns and actually

5
00:00:09,880 --> 00:00:14,759
the inventor of the telephone Alexander

6
00:00:12,280 --> 00:00:18,000
granell Einstein answered the phone

7
00:00:14,759 --> 00:00:21,119
Einstein and Koreans answer the phone

8
00:00:18,000 --> 00:00:23,480
yabo well for this class we've got

9
00:00:21,119 --> 00:00:26,039
conventions for calling of functions and

10
00:00:23,480 --> 00:00:27,720
there's three elements of importance how

11
00:00:26,039 --> 00:00:30,160
values are returned from called

12
00:00:27,720 --> 00:00:32,439
functions how values are passed from

13
00:00:30,160 --> 00:00:34,920
called functions and the register

14
00:00:32,439 --> 00:00:37,040
conventions of which registers can be

15
00:00:34,920 --> 00:00:38,800
smashed or should not be smashed when

16
00:00:37,040 --> 00:00:40,680
you call to a function I'm going to

17
00:00:38,800 --> 00:00:43,280
start with the easiest one how they're

18
00:00:40,680 --> 00:00:45,239
returned from functions so we've already

19
00:00:43,280 --> 00:00:48,840
seen this we've already seen main

20
00:00:45,239 --> 00:00:50,960
returning 41 or scalable and later on in

21
00:00:48,840 --> 00:00:54,239
the class we'll actually see returning

22
00:00:50,960 --> 00:00:57,079
values like a 128bit integer which can't

23
00:00:54,239 --> 00:01:00,000
even fit into a single register but we

24
00:00:57,079 --> 00:01:02,840
learned before that the return values

25
00:01:00,000 --> 00:01:05,000
are returned in the a z register and I

26
00:01:02,840 --> 00:01:07,240
will tell you for the future that if you

27
00:01:05,000 --> 00:01:10,040
have some value that's too big for a

28
00:01:07,240 --> 00:01:12,360
64bit register and 64-bit code or too

29
00:01:10,040 --> 00:01:16,280
big for a 32-bit register and 32-bit

30
00:01:12,360 --> 00:01:18,439
code then the a z and a1 register will

31
00:01:16,280 --> 00:01:20,680
be used to pass with the least

32
00:01:18,439 --> 00:01:23,200
significant bits going in the a Zer and

33
00:01:20,680 --> 00:01:25,000
the most significant bits in the a1 now

34
00:01:23,200 --> 00:01:28,000
where does this come from this comes

35
00:01:25,000 --> 00:01:30,200
from the RISC-V ABI documentation which

36
00:01:28,000 --> 00:01:31,640
tells us that the base integer calling

37
00:01:30,200 --> 00:01:33,920
convention provides eight argument

38
00:01:31,640 --> 00:01:36,799
registers a0 through A7 and their

39
00:01:33,920 --> 00:01:39,560
argument registers but the first two of

40
00:01:36,799 --> 00:01:41,520
which are also used to return values so

41
00:01:39,560 --> 00:01:43,360
I started with how to return values

42
00:01:41,520 --> 00:01:45,920
rather than how to pass values just

43
00:01:43,360 --> 00:01:48,520
because it's simpler check and we're

44
00:01:45,920 --> 00:01:51,240
done now let's focus on how to pass

45
00:01:48,520 --> 00:01:52,920
values into functions arguments are

46
00:01:51,240 --> 00:01:54,719
passed into functions using that thing

47
00:01:52,920 --> 00:01:56,520
we just saw right there a base integer

48
00:01:54,719 --> 00:01:59,920
calling convention provides eight

49
00:01:56,520 --> 00:02:01,920
argument registers a0 through A7 but the

50
00:01:59,920 --> 00:02:04,000
doesn't exactly tell us which arguments

51
00:02:01,920 --> 00:02:06,520
are passed in which order so I'm going

52
00:02:04,000 --> 00:02:09,840
to tell you that things are passed from

53
00:02:06,520 --> 00:02:12,000
left to right a z to A7 concretely let's

54
00:02:09,840 --> 00:02:15,800
say you were calling a function and it

55
00:02:12,000 --> 00:02:18,120
took 10 possible parameters well the

56
00:02:15,800 --> 00:02:20,959
zeroth parameter would go into a Zer the

57
00:02:18,120 --> 00:02:23,280
oneth into a1 and so forth all the way

58
00:02:20,959 --> 00:02:26,440
up until A7 so these first eight

59
00:02:23,280 --> 00:02:29,239
parameters directly from left to right

60
00:02:26,440 --> 00:02:31,200
go into the corresponding registers but

61
00:02:29,239 --> 00:02:33,280
what about those remaining values what

62
00:02:31,200 --> 00:02:36,560
if you have more than eight values

63
00:02:33,280 --> 00:02:38,640
passed into a function in that case the

64
00:02:36,560 --> 00:02:43,080
extra arguments are going to be passed

65
00:02:38,640 --> 00:02:45,239
on the stack so 88 goes here and 99 goes

66
00:02:43,080 --> 00:02:47,360
here so basically it's going to be from

67
00:02:45,239 --> 00:02:49,599
left to right the leftmost values will

68
00:02:47,360 --> 00:02:51,720
be at lower addresses and the rightmost

69
00:02:49,599 --> 00:02:53,280
values will be at higher addresses we

70
00:02:51,720 --> 00:02:55,360
don't even know what that is yet we'll

71
00:02:53,280 --> 00:02:57,239
see that later on in this section so

72
00:02:55,360 --> 00:02:59,440
under normal circumstances of course

73
00:02:57,239 --> 00:03:01,519
most things don't take 10 arguments to a

74
00:02:59,440 --> 00:03:03,280
function so most of the time you're

75
00:03:01,519 --> 00:03:05,760
going to get away with passing arguments

76
00:03:03,280 --> 00:03:07,599
and registers but sometimes they will be

77
00:03:05,760 --> 00:03:09,640
found on the stack and where does that

78
00:03:07,599 --> 00:03:11,879
information come from well going back to

79
00:03:09,640 --> 00:03:14,640
the ABI document again it says that

80
00:03:11,879 --> 00:03:16,640
scalers that are at most X length bits

81
00:03:14,640 --> 00:03:18,720
wide are passed in a single argument

82
00:03:16,640 --> 00:03:20,799
register or on the stack by value if

83
00:03:18,720 --> 00:03:22,680
none available I didn't highlight it but

84
00:03:20,799 --> 00:03:24,640
I'll also point out that when passed in

85
00:03:22,680 --> 00:03:27,680
registers or on the stack integer

86
00:03:24,640 --> 00:03:29,760
scalers is narrower than XLEN bits so

87
00:03:27,680 --> 00:03:33,040
if we're in 64-bit code and we're

88
00:03:29,760 --> 00:03:35,680
passing a 32-bit value or in 32-bit code

89
00:03:33,040 --> 00:03:38,280
and passing a 16bit value scalers that

90
00:03:35,680 --> 00:03:40,319
are narrower than XLEN bits are widened

91
00:03:38,280 --> 00:03:42,640
according to their sign meaning that if

92
00:03:40,319 --> 00:03:44,640
it's a signed variable it will be sign

93
00:03:42,640 --> 00:03:46,400
extended with all ones in the upper bits

94
00:03:44,640 --> 00:03:48,480
and if it's unsigned then it'll be all

95
00:03:46,400 --> 00:03:51,519
zeros in the upper bits widened

96
00:03:48,480 --> 00:03:54,799
according to their sign up to the type

97
00:03:51,519 --> 00:03:57,480
of 32 bits and then sign extended to

98
00:03:54,799 --> 00:03:59,000
xlink bits all right there's an

99
00:03:57,480 --> 00:04:00,920
additional little bit of information

100
00:03:59,000 --> 00:04:02,360
elsewhere in the the ABI that says the

101
00:04:00,920 --> 00:04:04,640
stack grows downward we know that

102
00:04:02,360 --> 00:04:06,640
already we've seen that already but the

103
00:04:04,640 --> 00:04:09,120
first argument passed on the stack is

104
00:04:06,640 --> 00:04:11,599
located at offset zero of the stack

105
00:04:09,120 --> 00:04:13,680
pointer on function entry and following

106
00:04:11,599 --> 00:04:15,480
arguments are stored at correspondingly

107
00:04:13,680 --> 00:04:18,320
higher addresses so that's what I was

108
00:04:15,480 --> 00:04:21,320
just showing in that diagram 88 was at a

109
00:04:18,320 --> 00:04:23,320
lower address and 99 was at a higher

110
00:04:21,320 --> 00:04:25,560
address now there's actually a whole

111
00:04:23,320 --> 00:04:28,040
bunch more rules in the ABI

112
00:04:25,560 --> 00:04:29,800
documentation and we will see all of the

113
00:04:28,040 --> 00:04:31,919
rules that I've just told you about in

114
00:04:29,800 --> 00:04:33,720
our examples and later on in the class

115
00:04:31,919 --> 00:04:35,720
we'll teach you how to read the fund

116
00:04:33,720 --> 00:04:38,400
manual so you can come back and enjoy

117
00:04:35,720 --> 00:04:41,400
the ABI at your leisure so that's how

118
00:04:38,400 --> 00:04:43,680
values our P to functions just the

119
00:04:41,400 --> 00:04:46,199
argument zero through argument 7 and the

120
00:04:43,680 --> 00:04:48,840
stack if necessary moving on to register

121
00:04:46,199 --> 00:04:51,440
conventions of what belongs to what so

122
00:04:48,840 --> 00:04:54,600
there's a notion of call Save registers

123
00:04:51,440 --> 00:04:57,320
and caller save registers and the way I

124
00:04:54,600 --> 00:04:59,840
say it is that a call E save register

125
00:04:57,320 --> 00:05:02,800
actually belongs to the caller

126
00:04:59,840 --> 00:05:04,639
consequently if the col needs to use

127
00:05:02,800 --> 00:05:06,759
these registers that notionally belong

128
00:05:04,639 --> 00:05:09,280
to the caller then it's the col's

129
00:05:06,759 --> 00:05:10,800
responsibility to save the values so

130
00:05:09,280 --> 00:05:13,000
that it doesn't break anything for the

131
00:05:10,800 --> 00:05:14,960
caller basically the caller should just

132
00:05:13,000 --> 00:05:17,280
depend upon the fact that certain

133
00:05:14,960 --> 00:05:19,840
registers will not be smashed by the col

134
00:05:17,280 --> 00:05:22,360
e and that it is the responsibility of

135
00:05:19,840 --> 00:05:23,840
the col e to save and restore them so

136
00:05:22,360 --> 00:05:25,960
the caller doesn't know that they even

137
00:05:23,840 --> 00:05:28,240
changed another way to put this and the

138
00:05:25,960 --> 00:05:31,039
way that the RISC-V ABI documentation

139
00:05:28,240 --> 00:05:33,440
says it is that saved registers are

140
00:05:31,039 --> 00:05:35,560
preserved across procedure calls so

141
00:05:33,440 --> 00:05:37,120
you've got some set of registers and you

142
00:05:35,560 --> 00:05:39,199
call a function you return from a

143
00:05:37,120 --> 00:05:41,160
function they should have the exact same

144
00:05:39,199 --> 00:05:43,360
value but the way that that's actually

145
00:05:41,160 --> 00:05:45,960
achieved in practice is that when you

146
00:05:43,360 --> 00:05:47,880
call into the function the call E saves

147
00:05:45,960 --> 00:05:49,280
them if necessary if it doesn't touch

148
00:05:47,880 --> 00:05:51,199
them then it doesn't have to do anything

149
00:05:49,280 --> 00:05:54,520
but if it needs to then it needs to save

150
00:05:51,199 --> 00:05:58,160
and restore so concretely the call E

151
00:05:54,520 --> 00:06:01,919
save registers in Risk 5 API is s0

152
00:05:58,160 --> 00:06:06,000
through S11 so the S was save save

153
00:06:01,919 --> 00:06:08,759
registers so s0 through S11 and stack

154
00:06:06,000 --> 00:06:11,560
pointer and again the documentation says

155
00:06:08,759 --> 00:06:14,160
s0 through S11 shall be preserved across

156
00:06:11,560 --> 00:06:16,560
procedure calls but I think this ABI

157
00:06:14,160 --> 00:06:18,919
mnemonic guide was the most helpful

158
00:06:16,560 --> 00:06:21,919
thing for Consulting of is something

159
00:06:18,919 --> 00:06:25,160
collie saved or caller saved so

160
00:06:21,919 --> 00:06:28,080
specifically if it says yes in the

161
00:06:25,160 --> 00:06:30,280
preserved across calls then that is a

162
00:06:28,080 --> 00:06:32,160
Col saved register of course it also

163
00:06:30,280 --> 00:06:35,120
just helpfully says right here that the

164
00:06:32,160 --> 00:06:37,120
s for the colie saved registers is a

165
00:06:35,120 --> 00:06:39,919
colie saved register but looking at the

166
00:06:37,120 --> 00:06:42,280
yeses in this column we also see the

167
00:06:39,919 --> 00:06:45,240
stack pointer so the stack pointer

168
00:06:42,280 --> 00:06:46,840
should be preserved by the col e if it

169
00:06:45,240 --> 00:06:49,000
needs to change it which of course most

170
00:06:46,840 --> 00:06:50,840
of the time it does most of the time if

171
00:06:49,000 --> 00:06:52,560
it needs to change it it must save it

172
00:06:50,840 --> 00:06:54,160
and then restore it at the end and we've

173
00:06:52,560 --> 00:06:56,400
seen that multiple times throughout this

174
00:06:54,160 --> 00:06:58,479
class already so those are call E save

175
00:06:56,400 --> 00:07:01,599
registers so everything else must be a

176
00:06:58,479 --> 00:07:04,759
caller save register right basically yes

177
00:07:01,599 --> 00:07:07,120
so caller save registers they belong to

178
00:07:04,759 --> 00:07:09,639
the call E in some sense and therefore

179
00:07:07,120 --> 00:07:11,840
it is the caller's responsibility to

180
00:07:09,639 --> 00:07:14,800
save the registers before it calls to a

181
00:07:11,840 --> 00:07:16,560
sub routine and if and only if it needs

182
00:07:14,800 --> 00:07:18,400
to ensure that they don't change and

183
00:07:16,560 --> 00:07:20,960
then it has to restore the register

184
00:07:18,400 --> 00:07:23,720
after the call returns and the RISC-V

185
00:07:20,960 --> 00:07:27,440
ABI says that caller save registers are

186
00:07:23,720 --> 00:07:30,160
not preserved across calls so concretely

187
00:07:27,440 --> 00:07:33,639
it is the temporary 0 through 6 t0

188
00:07:30,160 --> 00:07:35,360
through 6 and argument 0 through 7 and

189
00:07:33,639 --> 00:07:38,000
the return address which are call or

190
00:07:35,360 --> 00:07:40,360
save registers so circling it we've got

191
00:07:38,000 --> 00:07:42,479
the temporary registers so those are

192
00:07:40,360 --> 00:07:44,080
temporary and the caller should not

193
00:07:42,479 --> 00:07:46,080
assume that the call E isn't going to

194
00:07:44,080 --> 00:07:48,280
use them the call E may need some

195
00:07:46,080 --> 00:07:50,120
temporary registers of its own so if the

196
00:07:48,280 --> 00:07:52,280
caller wants to make sure that the call

197
00:07:50,120 --> 00:07:53,960
E doesn't smash them the caller needs to

198
00:07:52,280 --> 00:07:56,199
save and restore them before and after

199
00:07:53,960 --> 00:07:58,879
procedure calls again looking at the

200
00:07:56,199 --> 00:08:02,000
nose in this column we also have those

201
00:07:58,879 --> 00:08:04,800
arguments registers a0 through A7 we

202
00:08:02,000 --> 00:08:08,720
talked about how argument Z and argument

203
00:08:04,800 --> 00:08:11,159
1 a z and a1 are actually used to return

204
00:08:08,720 --> 00:08:14,159
values so this kind of makes sense then

205
00:08:11,159 --> 00:08:16,560
that argument registers they are set

206
00:08:14,159 --> 00:08:18,360
before calling into a function but the

207
00:08:16,560 --> 00:08:20,080
caller should not assume that they're

208
00:08:18,360 --> 00:08:22,280
not going to be changed indeed the a z

209
00:08:20,080 --> 00:08:24,960
and a1 will be changed if there's any

210
00:08:22,280 --> 00:08:26,960
sort of return and just in general the

211
00:08:24,960 --> 00:08:29,759
ABI says these are not preserved across

212
00:08:26,960 --> 00:08:32,120
calls therefore the call lead

213
00:08:29,759 --> 00:08:34,240
can use these sort of as temporary saved

214
00:08:32,120 --> 00:08:36,959
registers if they'd like as well and

215
00:08:34,240 --> 00:08:39,839
indeed we actually see that GCC seems to

216
00:08:36,959 --> 00:08:42,760
have a real thing for using things like

217
00:08:39,839 --> 00:08:45,279
a4 and a5 as its temporary scratch

218
00:08:42,760 --> 00:08:47,839
registers looking for more NOS in this

219
00:08:45,279 --> 00:08:49,480
column we also have the return address

220
00:08:47,839 --> 00:08:51,800
so in this section we're going to be

221
00:08:49,480 --> 00:08:53,959
focusing on calling two functions so

222
00:08:51,800 --> 00:08:56,320
we're going to see a lot of usage of the

223
00:08:53,959 --> 00:08:58,640
return address and so now we've seen all

224
00:08:56,320 --> 00:09:00,440
the caller and caller e save registers

225
00:08:58,640 --> 00:09:03,200
we've got Z which is neither cuz it's

226
00:09:00,440 --> 00:09:04,519
always hardcoded and then the ABI

227
00:09:03,200 --> 00:09:06,040
defines the global pointer and the

228
00:09:04,519 --> 00:09:08,000
thread pointer which we said we don't

229
00:09:06,040 --> 00:09:11,279
actually really see the usage of in this

230
00:09:08,000 --> 00:09:12,959
class as unallocate so essentially we

231
00:09:11,279 --> 00:09:14,959
don't know whether those are treated as

232
00:09:12,959 --> 00:09:16,279
collie or caller save registers and we

233
00:09:14,959 --> 00:09:18,200
don't care because we're not actually

234
00:09:16,279 --> 00:09:20,200
seeing them in this class the key

235
00:09:18,200 --> 00:09:22,600
principle though with collie and caller

236
00:09:20,200 --> 00:09:24,519
save registers is balance keeping things

237
00:09:22,600 --> 00:09:26,680
balanced as all things should be

238
00:09:24,519 --> 00:09:29,320
ultimately both the caller and the call

239
00:09:26,680 --> 00:09:31,279
E are responsible for balancing any

240
00:09:29,320 --> 00:09:33,680
saves that they do where here a save

241
00:09:31,279 --> 00:09:35,320
means storing something on the stack if

242
00:09:33,680 --> 00:09:37,959
you put something on the stack you

243
00:09:35,320 --> 00:09:40,079
better remove it later on and that is

244
00:09:37,959 --> 00:09:42,440
ultimately the register calling

245
00:09:40,079 --> 00:09:44,079
conventions of what belongs to what so

246
00:09:42,440 --> 00:09:45,519
these are the principles and now we're

247
00:09:44,079 --> 00:09:47,640
going to see those principles in

248
00:09:45,519 --> 00:09:49,839
practice as we go through some examples

249
00:09:47,640 --> 00:09:51,839
of calling to functions first without

250
00:09:49,839 --> 00:09:54,040
any arguments then calling multiple

251
00:09:51,839 --> 00:09:56,519
functions then calling functions with

252
00:09:54,040 --> 00:09:58,360
arguments and ultimately we'll see bits

253
00:09:56,519 --> 00:10:00,720
of this now I will say that for the

254
00:09:58,360 --> 00:10:02,640
register convention around caller and

255
00:10:00,720 --> 00:10:05,000
call eave registers we're not going to

256
00:10:02,640 --> 00:10:06,720
see too much of that because actually

257
00:10:05,000 --> 00:10:09,480
there are plenty of registers that's one

258
00:10:06,720 --> 00:10:11,959
of the benefits of having 32 registers

259
00:10:09,480 --> 00:10:13,720
total is that in general the compiler

260
00:10:11,959 --> 00:10:16,160
has plenty of registers to juggle

261
00:10:13,720 --> 00:10:18,720
temporary values before you know as it

262
00:10:16,160 --> 00:10:21,360
needs to calculate things and ultimately

263
00:10:18,720 --> 00:10:23,920
save values to memory we are running

264
00:10:21,360 --> 00:10:25,800
mostly very simple code consequently

265
00:10:23,920 --> 00:10:28,680
there's not too much juggling going on

266
00:10:25,800 --> 00:10:31,200
for the compiler and very infrequently

267
00:10:28,680 --> 00:10:33,399
will we see the explicit use of caller

268
00:10:31,200 --> 00:10:35,160
and call E save registers other than

269
00:10:33,399 --> 00:10:39,040
some of those special cases like stack

270
00:10:35,160 --> 00:10:39,040
pointers and return addresses

