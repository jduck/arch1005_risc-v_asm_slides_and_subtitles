1
00:00:00,040 --> 00:00:04,120
so the very important takeaways from

2
00:00:01,920 --> 00:00:06,359
this example is that pseudo instructions

3
00:00:04,120 --> 00:00:08,760
Walk Among Us they're nearly impossible

4
00:00:06,359 --> 00:00:12,839
to destroy they threaten our way of life

5
00:00:08,760 --> 00:00:15,719
and they poison our precious bodily

6
00:00:12,839 --> 00:00:18,240
fluids I just I don't know why the RISC

7
00:00:15,719 --> 00:00:21,480
5 people would do this to us anyways

8
00:00:18,240 --> 00:00:23,519
down here we have Bez and compressed Bez

9
00:00:21,480 --> 00:00:25,960
attaching themselves like parasites to

10
00:00:23,519 --> 00:00:29,599
our elegant control flow block quite

11
00:00:25,960 --> 00:00:29,599
frankly it makes me sick

