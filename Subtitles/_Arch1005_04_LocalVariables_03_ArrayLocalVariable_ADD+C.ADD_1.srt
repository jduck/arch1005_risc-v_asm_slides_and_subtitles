1
00:00:00,359 --> 00:00:05,560
now thus far we've only looked at simple

2
00:00:02,480 --> 00:00:07,919
scaler type variables in C we're going

3
00:00:05,560 --> 00:00:10,519
to start introducing more complicated

4
00:00:07,919 --> 00:00:13,200
compound types specifically arrays for

5
00:00:10,519 --> 00:00:16,359
now so we're going to have an array of

6
00:00:13,200 --> 00:00:18,800
six long Longs and then a single uh long

7
00:00:16,359 --> 00:00:21,160
long B we're going to initialize B we're

8
00:00:18,800 --> 00:00:23,599
going to initialize a of one and we're

9
00:00:21,160 --> 00:00:25,880
going to do some math on a of 1 plus B

10
00:00:23,599 --> 00:00:27,920
Store the value into a of four and

11
00:00:25,880 --> 00:00:30,160
return a of four as the long long return

12
00:00:27,920 --> 00:00:33,680
value now for this code I'm going going

13
00:00:30,160 --> 00:00:36,360
to introduce the use of this F no stack

14
00:00:33,680 --> 00:00:38,559
protector option for the compiler and

15
00:00:36,360 --> 00:00:40,640
this is because basically the compiler

16
00:00:38,559 --> 00:00:42,239
said hey you've got an array here I

17
00:00:40,640 --> 00:00:45,160
think you might accidentally cause a

18
00:00:42,239 --> 00:00:47,160
buffer overflow so it introduces a stack

19
00:00:45,160 --> 00:00:49,600
protector also known as stack cookie or

20
00:00:47,160 --> 00:00:50,920
stack Canary it introduces that into the

21
00:00:49,600 --> 00:00:52,520
code and that's going to make the code

22
00:00:50,920 --> 00:00:54,039
much longer it's going to introduce new

23
00:00:52,520 --> 00:00:56,760
assembly instructions that were not

24
00:00:54,039 --> 00:00:59,160
ready to cover yet so I added no stack

25
00:00:56,760 --> 00:01:00,920
protector to turn off the security and

26
00:00:59,160 --> 00:01:04,080
force it to gener generate more simple

27
00:01:00,920 --> 00:01:06,840
code for optimization level zero so with

28
00:01:04,080 --> 00:01:09,240
that here's our popup of code and we

29
00:01:06,840 --> 00:01:11,759
have a new assembly instruction to cover

30
00:01:09,240 --> 00:01:14,960
add not add I which we've seen many

31
00:01:11,759 --> 00:01:17,880
times before but just plain add and what

32
00:01:14,960 --> 00:01:20,640
does add do well it is taking source one

33
00:01:17,880 --> 00:01:24,920
plus source 2 and storing it into the

34
00:01:20,640 --> 00:01:28,040
destination register simple elegant ad

35
00:01:24,920 --> 00:01:30,280
so for instance x2 + x3 into x1 that's

36
00:01:28,040 --> 00:01:33,240
how that would be read but if it's worth

37
00:01:30,280 --> 00:01:35,600
saying that normal RV32I those

38
00:01:33,240 --> 00:01:37,600
instructions usually don't have two

39
00:01:35,600 --> 00:01:40,560
operand forms so they always require you

40
00:01:37,600 --> 00:01:42,439
to say two operands to be added

41
00:01:40,560 --> 00:01:44,479
subtracted multiplied whatever two

42
00:01:42,439 --> 00:01:46,119
operands and a destination operand

43
00:01:44,479 --> 00:01:48,799
whereas some other architectures like

44
00:01:46,119 --> 00:01:50,840
x86 may only require a source and a

45
00:01:48,799 --> 00:01:53,240
destination and then implicitly the

46
00:01:50,840 --> 00:01:56,200
destination is also a source so for now

47
00:01:53,240 --> 00:01:59,039
for normal RV32I we're going to have

48
00:01:56,200 --> 00:02:01,960
all three operands so yep nothing like

49
00:01:59,039 --> 00:02:04,880
this know our destination implicitly

50
00:02:01,960 --> 00:02:06,399
being a source okay well you've done it

51
00:02:04,880 --> 00:02:07,439
before and you're going to do it again

52
00:02:06,399 --> 00:02:09,599
and you're going to do it many more

53
00:02:07,439 --> 00:02:12,520
times after this it's time to go ahead

54
00:02:09,599 --> 00:02:14,840
and throw this compiled output into the

55
00:02:12,520 --> 00:02:17,519
debugger and figure out what the stack

56
00:02:14,840 --> 00:02:19,959
looks like for these new compound arrays

57
00:02:17,519 --> 00:02:23,000
so where is array of zero where is array

58
00:02:19,959 --> 00:02:25,120
of 1 4 B you got to figure that out

59
00:02:23,000 --> 00:02:27,319
based on stepping through the coat so go

60
00:02:25,120 --> 00:02:28,879
ahead and stop and step through the

61
00:02:27,319 --> 00:02:31,760
assembly and draw yourself a stack

62
00:02:28,879 --> 00:02:31,760
diagram

