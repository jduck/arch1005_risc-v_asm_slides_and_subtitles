1
00:00:00,040 --> 00:00:02,720
now I don't want to talk to you about

2
00:00:01,120 --> 00:00:05,279
the power of love I want to talk to you

3
00:00:02,720 --> 00:00:08,000
about the power of sign the power of

4
00:00:05,279 --> 00:00:11,599
sign is a Curious Thing make a oneman

5
00:00:08,000 --> 00:00:12,960
weep make another man sing yes again I

6
00:00:11,599 --> 00:00:15,040
reserve the right to break in a song at

7
00:00:12,960 --> 00:00:17,880
any point in this class and indeed the

8
00:00:15,040 --> 00:00:20,439
power of sign is about integer overflow

9
00:00:17,880 --> 00:00:22,279
vulnerabilities these signs can cause

10
00:00:20,439 --> 00:00:24,279
the developers to weep and the

11
00:00:22,279 --> 00:00:26,359
exploiters to sing you're going to have

12
00:00:24,279 --> 00:00:28,320
to go take the OST2 vulnerabilities

13
00:00:26,359 --> 00:00:30,519
class though to find out why in the

14
00:00:28,320 --> 00:00:32,360
meantime we're just going to take that

15
00:00:30,519 --> 00:00:35,280
example we just looked at and change it

16
00:00:32,360 --> 00:00:37,000
from an unsigned long to a signed long

17
00:00:35,280 --> 00:00:39,280
so as a reminder this is what we just

18
00:00:37,000 --> 00:00:42,399
saw when we had unsigned long silly and

19
00:00:39,280 --> 00:00:45,800
sirly and now if we compile this new

20
00:00:42,399 --> 00:00:48,480
signed code we get this instead still

21
00:00:45,800 --> 00:00:52,239
silly but now we've got something else

22
00:00:48,480 --> 00:00:55,800
Sarai or I'm going to say sray like

23
00:00:52,239 --> 00:00:58,559
x-rays so new assembly instruction sray

24
00:00:55,800 --> 00:01:00,680
sray is shift right arithmetic by

25
00:00:58,559 --> 00:01:02,879
immediate so no longer are we shifting

26
00:01:00,680 --> 00:01:05,280
right logical something new called

27
00:01:02,879 --> 00:01:08,040
shifting right arithmetic and it behaves

28
00:01:05,280 --> 00:01:11,119
the same way as shift right so Surly

29
00:01:08,040 --> 00:01:14,080
shift right logical by immediate so

30
00:01:11,119 --> 00:01:15,960
Surly had the 5 bit and six-bit forms

31
00:01:14,080 --> 00:01:18,360
this is now arithmetic instead of

32
00:01:15,960 --> 00:01:20,280
logical so it shifts the same way but

33
00:01:18,360 --> 00:01:22,960
the key thing is that an arithmetic

34
00:01:20,280 --> 00:01:25,960
shift fills in the shifted bits not with

35
00:01:22,960 --> 00:01:28,200
just always zeros but instead with

36
00:01:25,960 --> 00:01:30,960
whatever the previous most significant

37
00:01:28,200 --> 00:01:35,159
bit was so let's see that concretely so

38
00:01:30,960 --> 00:01:37,240
let's say that we have sray a4 a5 and 12

39
00:01:35,159 --> 00:01:39,119
a5 is going to be staba Abid dudes but

40
00:01:37,240 --> 00:01:41,840
the key thing is that stabit Abid dudes

41
00:01:39,119 --> 00:01:43,520
has a zero in its most significant bit

42
00:01:41,840 --> 00:01:45,520
so shift it by 12 and you're going to

43
00:01:43,520 --> 00:01:48,560
see exactly the same stuff as when you

44
00:01:45,520 --> 00:01:52,040
had a logical shift because all the bits

45
00:01:48,560 --> 00:01:54,960
the top bits are shifted over but also

46
00:01:52,040 --> 00:01:57,560
the top bit is placed as the most

47
00:01:54,960 --> 00:01:59,479
significant 12 bits so if we instead

48
00:01:57,560 --> 00:02:01,880
pick a different example where we start

49
00:01:59,479 --> 00:02:06,280
out with with do

50
00:02:01,880 --> 00:02:09,039
dodoo as our a5 then because one is the

51
00:02:06,280 --> 00:02:11,239
most significant bit it will shift over

52
00:02:09,039 --> 00:02:13,640
all the bits by 12 but then it will

53
00:02:11,239 --> 00:02:15,480
spread that most significant bit as the

54
00:02:13,640 --> 00:02:17,720
new 12 bits that are the most

55
00:02:15,480 --> 00:02:19,680
significant bits so that is the

56
00:02:17,720 --> 00:02:22,000
difference between arithmetic shifting

57
00:02:19,680 --> 00:02:23,800
and logical shifting it's just in

58
00:02:22,000 --> 00:02:26,040
arithmetic you spread whatever the most

59
00:02:23,800 --> 00:02:28,160
significant bit is as the new thing that

60
00:02:26,040 --> 00:02:30,400
is copied into the higher order bits

61
00:02:28,160 --> 00:02:33,160
after the shifting whereas in logical

62
00:02:30,400 --> 00:02:35,120
shifts it's always just zero okay go

63
00:02:33,160 --> 00:02:39,680
compile the code look at the code and

64
00:02:35,120 --> 00:02:39,680
make sure you understand what it's doing

