1
00:00:00,080 --> 00:00:05,279
so if you stepped through main plus 56

2
00:00:02,600 --> 00:00:07,600
the last store to the stack you should

3
00:00:05,279 --> 00:00:09,760
see something like this all The Usual

4
00:00:07,600 --> 00:00:11,440
Suspects saved frame pointers

5
00:00:09,760 --> 00:00:13,000
uninitialized space for alignment

6
00:00:11,440 --> 00:00:16,119
uninitialized space for alignment at the

7
00:00:13,000 --> 00:00:19,960
bottom but now when we look at it we see

8
00:00:16,119 --> 00:00:22,560
B A and C so we can see that they're not

9
00:00:19,960 --> 00:00:25,279
actually allocated in the same order as

10
00:00:22,560 --> 00:00:28,279
we saw in the source code so bboa still

11
00:00:25,279 --> 00:00:30,039
bled blood and was stored into CA of2

12
00:00:28,279 --> 00:00:31,640
but that's not necessarily where we

13
00:00:30,039 --> 00:00:33,719
would expect if we expected them to be

14
00:00:31,640 --> 00:00:36,120
in all the same order so that is our

15
00:00:33,719 --> 00:00:40,680
takeaway from this array local variable

16
00:00:36,120 --> 00:00:42,600
to o0 no cookie it is that the compiler

17
00:00:40,680 --> 00:00:44,280
could actually rearrange stuff and it

18
00:00:42,600 --> 00:00:46,360
seems like it's going to do that more

19
00:00:44,280 --> 00:00:48,160
when it's dealing with arrays than when

20
00:00:46,360 --> 00:00:49,960
we deal with scalers as we've seen in

21
00:00:48,160 --> 00:00:51,840
the past the other minor point because I

22
00:00:49,960 --> 00:00:53,879
didn't say it already is that a was

23
00:00:51,840 --> 00:00:56,920
fully initialized to zeros because of

24
00:00:53,879 --> 00:00:59,480
that syntax with the equals and then the

25
00:00:56,920 --> 00:01:01,640
empty curly braces but again the main

26
00:00:59,480 --> 00:01:03,559
takeaway I want you to take from this is

27
00:01:01,640 --> 00:01:05,360
the fact that the compiler could

28
00:01:03,559 --> 00:01:07,280
rearrange things it's not actually

29
00:01:05,360 --> 00:01:10,759
required to put them in exactly the

30
00:01:07,280 --> 00:01:10,759
order they were declared

