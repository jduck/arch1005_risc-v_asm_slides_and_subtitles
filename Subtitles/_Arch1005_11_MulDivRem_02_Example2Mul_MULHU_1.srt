1
00:00:00,240 --> 00:00:07,639
example two M.C is the heximal hanal

2
00:00:03,719 --> 00:00:09,800
Lector of examples because dissected

3
00:00:07,639 --> 00:00:12,519
delectable but in this example we're

4
00:00:09,800 --> 00:00:14,360
going to use integer 128 so that's going

5
00:00:12,519 --> 00:00:16,960
to be something a little bit new and

6
00:00:14,360 --> 00:00:19,320
integer 128 GCC has support for this you

7
00:00:16,960 --> 00:00:23,039
do underscore uncore in

8
00:00:19,320 --> 00:00:26,039
128a initialize it to some hex value

9
00:00:23,039 --> 00:00:28,640
then we're going to multiply integer 128

10
00:00:26,039 --> 00:00:30,720
times another hexadecimal value and

11
00:00:28,640 --> 00:00:32,719
because of the way that C multiplication

12
00:00:30,720 --> 00:00:35,920
works it's going to upcast this to be

13
00:00:32,719 --> 00:00:38,760
128bit value so it's going to do 128 *

14
00:00:35,920 --> 00:00:41,360
128bit multiplication when we compile

15
00:00:38,760 --> 00:00:43,280
this at optimization level zero we get

16
00:00:41,360 --> 00:00:45,039
the following code which is actually

17
00:00:43,280 --> 00:00:46,840
pretty complicated given the fact that

18
00:00:45,039 --> 00:00:48,680
we just have two constants being

19
00:00:46,840 --> 00:00:50,199
multiplied by each other so I was

20
00:00:48,680 --> 00:00:53,079
searching for something that would give

21
00:00:50,199 --> 00:00:55,480
me some multiply that keeps the high

22
00:00:53,079 --> 00:00:57,359
order bits but little did I know when I

23
00:00:55,480 --> 00:00:59,039
stumbled on this example that I would be

24
00:00:57,359 --> 00:01:01,039
making things more complicated for

25
00:00:59,039 --> 00:01:03,280
myself by having to explain to you

26
00:01:01,039 --> 00:01:06,000
what's going on with 128bit

27
00:01:03,280 --> 00:01:08,680
multiplication when you have only 64 *

28
00:01:06,000 --> 00:01:10,119
64-bit multiplication instructions let's

29
00:01:08,680 --> 00:01:12,439
cover the instruction and come back to

30
00:01:10,119 --> 00:01:15,880
the intuition behind what's going on

31
00:01:12,439 --> 00:01:19,119
later so first thing multiply high

32
00:01:15,880 --> 00:01:21,680
unsigned multiply keep the high XLEN

33
00:01:19,119 --> 00:01:23,840
bits and treat it as if it's unsigned

34
00:01:21,680 --> 00:01:25,520
times unsigned multiplication earlier I

35
00:01:23,840 --> 00:01:27,000
had showed you the picture for four bit

36
00:01:25,520 --> 00:01:28,640
multiplication where we said the bottom

37
00:01:27,000 --> 00:01:30,400
four bits will always be the same

38
00:01:28,640 --> 00:01:32,280
regardless of the signs

39
00:01:30,400 --> 00:01:34,000
whereas the upper bits will be different

40
00:01:32,280 --> 00:01:36,520
based on unsigned versus signed

41
00:01:34,000 --> 00:01:38,799
multiplication so RISC-V consequently

42
00:01:36,520 --> 00:01:40,560
does different assembly instructions for

43
00:01:38,799 --> 00:01:42,680
whether you're operating on unsigned

44
00:01:40,560 --> 00:01:45,119
time unsigned signed time signed or

45
00:01:42,680 --> 00:01:47,280
unsigned times signed this one is the

46
00:01:45,119 --> 00:01:50,360
unsigned time unsigned so you've got

47
00:01:47,280 --> 00:01:53,119
your source operand 1 times your source

48
00:01:50,360 --> 00:01:55,960
operand 2 assume that they're unsigned

49
00:01:53,119 --> 00:01:58,719
and keep only the high XLEN bits not

50
00:01:55,960 --> 00:02:01,200
the low like just a plain M but the high

51
00:01:58,719 --> 00:02:05,159
XLEN bits so that's exactly what that's

52
00:02:01,200 --> 00:02:08,119
saying again just XLEN sized rs1 so this

53
00:02:05,159 --> 00:02:09,800
could be 32bit could be 64bit depends on

54
00:02:08,119 --> 00:02:12,280
how your code is compiled and what

55
00:02:09,800 --> 00:02:15,280
execution mode you're operating in so

56
00:02:12,280 --> 00:02:18,840
xll time XLEN store the high xll into the

57
00:02:15,280 --> 00:02:21,560
destination which is X length wi so I

58
00:02:18,840 --> 00:02:23,400
said that the multiply high unsign is me

59
00:02:21,560 --> 00:02:26,280
meant to be used when multiplying unsign

60
00:02:23,400 --> 00:02:29,560
times unsign and we know that we're

61
00:02:26,280 --> 00:02:32,160
actually doing a signed integer 128 a *

62
00:02:29,560 --> 00:02:34,360
it self so s time sign so why are we

63
00:02:32,160 --> 00:02:36,160
seeing this here well we'll come back to

64
00:02:34,360 --> 00:02:38,480
that after we cover a little bit more of

65
00:02:36,160 --> 00:02:40,319
the intuition after you see the actual

66
00:02:38,480 --> 00:02:42,000
assembly code step through every single

67
00:02:40,319 --> 00:02:44,120
line make notes for yourself on every

68
00:02:42,000 --> 00:02:46,560
single line and then you'll be clear on

69
00:02:44,120 --> 00:02:48,720
why this is happening so just to put an

70
00:02:46,560 --> 00:02:53,159
example out there we have M high

71
00:02:48,720 --> 00:02:56,480
unsigned t0 set to hex 20 which is 32

72
00:02:53,159 --> 00:02:59,319
times T1 which we're going to set to one

73
00:02:56,480 --> 00:03:03,200
and all zeros so that's going to be one

74
00:02:59,319 --> 00:03:06,120
and 15 zeros so the result of this

75
00:03:03,200 --> 00:03:08,879
because you have hex 20 and so you got

76
00:03:06,120 --> 00:03:10,799
15 zeros and 20 which has one zero

77
00:03:08,879 --> 00:03:13,280
you're going to need at least 16 zeros

78
00:03:10,799 --> 00:03:16,159
and then a two but that means that that

79
00:03:13,280 --> 00:03:18,720
result can't possibly fit in a 64-bit

80
00:03:16,159 --> 00:03:20,239
value so the net result of this is

81
00:03:18,720 --> 00:03:23,440
because we're doing the multiplication

82
00:03:20,239 --> 00:03:25,879
and keeping only the high the upper 64

83
00:03:23,440 --> 00:03:28,120
bits is just going to be the two and

84
00:03:25,879 --> 00:03:30,000
then if we do a mole to get the lower

85
00:03:28,120 --> 00:03:32,200
bits and store it into a different

86
00:03:30,000 --> 00:03:37,280
register so we don't clobber our T2 sort

87
00:03:32,200 --> 00:03:40,400
it into T3 we get X20 * 1 and 15 Z and

88
00:03:37,280 --> 00:03:42,840
the net result is all zeros the 16 zeros

89
00:03:40,400 --> 00:03:46,720
from the bottom zero and the 15 zeros

90
00:03:42,840 --> 00:03:51,159
here so the overall 128bit value of this

91
00:03:46,720 --> 00:03:54,799
64 * 64-bit multiplication is 2 and 16

92
00:03:51,159 --> 00:03:58,159
zos upper bits stored in T2 by the M

93
00:03:54,799 --> 00:04:00,640
high unsigned lower bits stored in T3 by

94
00:03:58,159 --> 00:04:02,640
the just plain Mall so returning to the

95
00:04:00,640 --> 00:04:04,840
code I said that this was just me trying

96
00:04:02,640 --> 00:04:07,640
to create something small that would you

97
00:04:04,840 --> 00:04:09,239
know put the store something into 128

98
00:04:07,640 --> 00:04:11,159
bit value so that we would keep the

99
00:04:09,239 --> 00:04:13,319
upper 64 bits that's all I was really

100
00:04:11,159 --> 00:04:14,920
trying to go for but be you know after

101
00:04:13,319 --> 00:04:16,919
playing around with various permutations

102
00:04:14,920 --> 00:04:19,239
and stuff this was the simple form

103
00:04:16,919 --> 00:04:22,040
simple in the sense of simple C but this

104
00:04:19,239 --> 00:04:25,479
was clearly not simple assembly we have

105
00:04:22,040 --> 00:04:28,600
multiple multiplications we have m m m

106
00:04:25,479 --> 00:04:30,960
and then M hu so I need to give you some

107
00:04:28,600 --> 00:04:35,080
intuition of how the compiler is going

108
00:04:30,960 --> 00:04:37,199
to generate a 128bit * 128bit

109
00:04:35,080 --> 00:04:39,759
multiplication I really just wanted to

110
00:04:37,199 --> 00:04:42,120
like handwave around all this and say ah

111
00:04:39,759 --> 00:04:44,039
you know whatever when you have a you

112
00:04:42,120 --> 00:04:45,320
know debugger support you just walk

113
00:04:44,039 --> 00:04:48,360
through with the debugger look at the

114
00:04:45,320 --> 00:04:50,199
values and it'll make sense but this may

115
00:04:48,360 --> 00:04:51,960
not have necessarily made sense so I did

116
00:04:50,199 --> 00:04:53,840
feel I had to give you a little more

117
00:04:51,960 --> 00:04:56,360
intuition before I turned you loose in

118
00:04:53,840 --> 00:04:58,680
the debugger to see what's going on here

119
00:04:56,360 --> 00:05:02,240
so the first point is that the compiler

120
00:04:58,680 --> 00:05:05,880
is doing a * a which is 128 bit * 128bit

121
00:05:02,240 --> 00:05:08,199
the result should be a 256 bit value but

122
00:05:05,880 --> 00:05:11,080
we're storing the result back into a the

123
00:05:08,199 --> 00:05:13,440
128bit value that means we don't need to

124
00:05:11,080 --> 00:05:15,560
even care about the upper 128 bits the

125
00:05:13,440 --> 00:05:18,400
compiler doesn't need to generate code

126
00:05:15,560 --> 00:05:20,759
to create those values for the upper 128

127
00:05:18,400 --> 00:05:23,919
bits they'll just be thrown away anyways

128
00:05:20,759 --> 00:05:27,080
so the compiler should only create code

129
00:05:23,919 --> 00:05:29,919
that generates the lower 128bit values

130
00:05:27,080 --> 00:05:32,360
that'll ultimately be stored in returned

131
00:05:29,919 --> 00:05:34,440
so now I am again borrowing some

132
00:05:32,360 --> 00:05:37,520
inspiration from this arm assembly for

133
00:05:34,440 --> 00:05:39,919
embedded applications book and if you go

134
00:05:37,520 --> 00:05:42,240
look at his chapter 5 integer arithmetic

135
00:05:39,919 --> 00:05:44,600
section he talks about 64-bit time

136
00:05:42,240 --> 00:05:48,400
64-bit multiplication in the context of

137
00:05:44,600 --> 00:05:50,560
arm that was only 32-bit arm and so I'm

138
00:05:48,400 --> 00:05:53,919
taking the same principle of we're

139
00:05:50,560 --> 00:05:55,639
trying to do multiplication of variables

140
00:05:53,919 --> 00:05:57,479
that are too big for us to even hold in

141
00:05:55,639 --> 00:05:59,600
a register and for which we have no

142
00:05:57,479 --> 00:06:01,639
native multiplication assembly

143
00:05:59,600 --> 00:06:04,039
instruction but the same principle that

144
00:06:01,639 --> 00:06:07,800
applies to 64 * 64 in the context of

145
00:06:04,039 --> 00:06:10,960
32-bit code can apply to 128 * 128 in

146
00:06:07,800 --> 00:06:13,080
the context of 64-bit code so first of

147
00:06:10,960 --> 00:06:17,199
all let's say that we have a variable a

148
00:06:13,080 --> 00:06:18,960
and a variable B which are 128 bits so a

149
00:06:17,199 --> 00:06:21,960
different mathematical way that we could

150
00:06:18,960 --> 00:06:25,319
represent a the 128bit value is as if it

151
00:06:21,960 --> 00:06:28,080
was two 64-bit values the first 64-bit

152
00:06:25,319 --> 00:06:31,280
value a high for the upper half is

153
00:06:28,080 --> 00:06:34,680
multiplied by two 64 if we think about

154
00:06:31,280 --> 00:06:38,880
that in heximal 2 64 would be like a 1

155
00:06:34,680 --> 00:06:41,000
followed by 16 zeros in heximal so

156
00:06:38,880 --> 00:06:42,960
taking any value let's call it dead beef

157
00:06:41,000 --> 00:06:46,080
Cafe babe dead beef Cafe babe

158
00:06:42,960 --> 00:06:48,520
multiplying it by 1 and 16 zeros in

159
00:06:46,080 --> 00:06:51,639
heximal is kind of just the same thing

160
00:06:48,520 --> 00:06:53,880
as shifting It Up by 64 bits so whatever

161
00:06:51,639 --> 00:06:56,520
your a high value was in heximal if you

162
00:06:53,880 --> 00:06:59,080
multiply it by 2 64 that'll shift it up

163
00:06:56,520 --> 00:07:01,680
it'll be represented up here and then

164
00:06:59,080 --> 00:07:04,599
plus plus the AL low value whatever that

165
00:07:01,680 --> 00:07:06,800
was will give you the overall 128bit

166
00:07:04,599 --> 00:07:09,120
value that is a so we're going to write

167
00:07:06,800 --> 00:07:12,240
it this way we're going to write B the

168
00:07:09,120 --> 00:07:16,120
same way of a b high and a b low B high

169
00:07:12,240 --> 00:07:18,400
* 2 64 and B low just added into the

170
00:07:16,120 --> 00:07:21,639
resulting value and so if we want to do

171
00:07:18,400 --> 00:07:24,199
a * B we're instead going to multiply

172
00:07:21,639 --> 00:07:29,039
these alternative representations of A

173
00:07:24,199 --> 00:07:31,000
and B so a right here times B right here

174
00:07:29,039 --> 00:07:33,360
where we we' just substituted in these

175
00:07:31,000 --> 00:07:35,879
values up here now at this point you

176
00:07:33,360 --> 00:07:38,919
want to do the multiplicative uh

177
00:07:35,879 --> 00:07:41,400
distributive law and you would multiply

178
00:07:38,919 --> 00:07:43,919
these things out to get a result now I'm

179
00:07:41,400 --> 00:07:46,360
just going to say my ordering is going

180
00:07:43,919 --> 00:07:47,919
to come from the little mnemonic that

181
00:07:46,360 --> 00:07:51,360
they teach in algebra in the United

182
00:07:47,919 --> 00:07:53,680
States called foil and so in foil you do

183
00:07:51,360 --> 00:07:58,639
the first things multiplied together

184
00:07:53,680 --> 00:08:00,000
plus the outer things first so first

185
00:07:58,639 --> 00:08:03,039
outer

186
00:08:00,000 --> 00:08:05,479
inner and then last foil is first outer

187
00:08:03,039 --> 00:08:09,919
inner last so the net result of doing

188
00:08:05,479 --> 00:08:13,360
that is going to be first a high * 2 64

189
00:08:09,919 --> 00:08:16,280
time the first thing B high * 2 64 it

190
00:08:13,360 --> 00:08:20,000
gives you a high time B high Times 2 to

191
00:08:16,280 --> 00:08:23,280
the 128 because the 2 to 64 and 2 to 64

192
00:08:20,000 --> 00:08:28,240
the 64s can just be added so that's the

193
00:08:23,280 --> 00:08:33,080
first the outer is a high * 2 64 * B low

194
00:08:28,240 --> 00:08:38,760
so a high * B low * 2 64 the inner a low

195
00:08:33,080 --> 00:08:41,440
* B high * 2 64 a low * B high * 2 64

196
00:08:38,760 --> 00:08:43,360
and then last a low time B low so it's

197
00:08:41,440 --> 00:08:45,560
the exact same thing as if you apply the

198
00:08:43,360 --> 00:08:47,519
proper generic distributive law it's

199
00:08:45,560 --> 00:08:48,800
just a neonic that they teach and that's

200
00:08:47,519 --> 00:08:52,640
the first thing that I thought when I

201
00:08:48,800 --> 00:08:55,120
saw this you know x + y * Z * W or

202
00:08:52,640 --> 00:08:57,600
whatever so this is going to be the

203
00:08:55,120 --> 00:09:00,200
result here now we get the

204
00:08:57,600 --> 00:09:02,720
multiplication of a high time B high

205
00:09:00,200 --> 00:09:05,760
that is a value that we can calculate

206
00:09:02,720 --> 00:09:09,519
with our 64-bit multiplication so a high

207
00:09:05,760 --> 00:09:12,760
upper 64 bits time be high upper 64 bits

208
00:09:09,519 --> 00:09:15,240
we have a 64 * 64 multiplication which

209
00:09:12,760 --> 00:09:17,519
we can get that and then the times to

210
00:09:15,240 --> 00:09:21,640
the 128 would kind of just be like as if

211
00:09:17,519 --> 00:09:24,959
it was shifted by 128 bits this way so

212
00:09:21,640 --> 00:09:27,720
that's our first result and then plus

213
00:09:24,959 --> 00:09:32,920
the next result which is a high * B low

214
00:09:27,720 --> 00:09:35,760
* 2 the 64 again a high * B low that is

215
00:09:32,920 --> 00:09:37,720
a 64 * 64-bit multiplication we have an

216
00:09:35,760 --> 00:09:40,120
assembly instruction for that we can

217
00:09:37,720 --> 00:09:42,680
calculate the product of that and the

218
00:09:40,120 --> 00:09:45,839
fact that it's time 2 64 is just like as

219
00:09:42,680 --> 00:09:49,040
if it was shifted up time 2 64 then the

220
00:09:45,839 --> 00:09:52,880
next product a low * B high is going to

221
00:09:49,040 --> 00:09:55,760
be added in so add in a low * B high * 2

222
00:09:52,880 --> 00:09:58,040
64 same thing we can calculate that we

223
00:09:55,760 --> 00:10:01,440
can get the lower 64 from the M

224
00:09:58,040 --> 00:10:03,279
instruction the upper 64 from the M hu

225
00:10:01,440 --> 00:10:06,320
instruction and so we can get the full

226
00:10:03,279 --> 00:10:09,920
128bit value there and then finally the

227
00:10:06,320 --> 00:10:11,680
Alo time bow we're going to add that in

228
00:10:09,920 --> 00:10:14,360
and that would be not shifted by

229
00:10:11,680 --> 00:10:16,440
anything it's just a low time bow but

230
00:10:14,360 --> 00:10:19,079
the key Insight of these four

231
00:10:16,440 --> 00:10:22,279
multiplications plus additions is that

232
00:10:19,079 --> 00:10:24,200
that would be a 100 or sorry a 256bit

233
00:10:22,279 --> 00:10:28,680
result but we said we don't need a

234
00:10:24,200 --> 00:10:29,800
256-bit result we need a 128bit result

235
00:10:28,680 --> 00:10:32,160
so

236
00:10:29,800 --> 00:10:34,240
really all of this stuff up here we

237
00:10:32,160 --> 00:10:36,880
don't need to compute that what we need

238
00:10:34,240 --> 00:10:40,360
to compute is this bottom 64 bits of

239
00:10:36,880 --> 00:10:43,839
that result plus the bottom 64 bits of

240
00:10:40,360 --> 00:10:45,600
that result plus the upper 64 bits of

241
00:10:43,839 --> 00:10:48,079
that result and that'll give us the

242
00:10:45,600 --> 00:10:50,240
upper 64 bits so just this plus this

243
00:10:48,079 --> 00:10:53,079
plus this store it somewhere that'll be

244
00:10:50,240 --> 00:10:55,120
the upper 64 bits and then just this

245
00:10:53,079 --> 00:10:57,880
multiplication and just keeping the

246
00:10:55,120 --> 00:11:01,560
bottom 64 bits will give us the bottom

247
00:10:57,880 --> 00:11:03,200
64 bits of the overall 128bit result so

248
00:11:01,560 --> 00:11:05,920
just to be more concrete about that in

249
00:11:03,200 --> 00:11:08,920
the context of our a times you know the

250
00:11:05,920 --> 00:11:12,040
other new constant the dissected would

251
00:11:08,920 --> 00:11:15,240
be the lower 64 bits to start with and

252
00:11:12,040 --> 00:11:17,360
then the 64-bit value that is upcast it

253
00:11:15,240 --> 00:11:19,639
is just delectable but it's upcast to

254
00:11:17,360 --> 00:11:22,720
128bit Value before it's multiplied

255
00:11:19,639 --> 00:11:26,240
times dissected and so we really only

256
00:11:22,720 --> 00:11:29,240
need to do this a high time B low so a

257
00:11:26,240 --> 00:11:32,440
high which is zeros time B low which is

258
00:11:29,240 --> 00:11:33,720
delectable keep only the lower bits and

259
00:11:32,440 --> 00:11:35,160
quite frankly because you can see it's

260
00:11:33,720 --> 00:11:39,160
zero you know the bits are going to be

261
00:11:35,160 --> 00:11:41,839
zero and then the a low time B high so a

262
00:11:39,160 --> 00:11:43,560
low time B high well again 0 time

263
00:11:41,839 --> 00:11:46,959
anything is zero so that's going to be

264
00:11:43,560 --> 00:11:49,720
zero and so then ultimately this a low

265
00:11:46,959 --> 00:11:51,639
time B low that is going to be m a

266
00:11:49,720 --> 00:11:54,800
combination of a normal multiply to get

267
00:11:51,639 --> 00:11:57,560
the lower bits and a multiply high to

268
00:11:54,800 --> 00:11:59,800
get the high bits and so adding this to

269
00:11:57,560 --> 00:12:02,240
this to this that's what we ultimately

270
00:11:59,800 --> 00:12:04,120
are seeing in this assembly so there's

271
00:12:02,240 --> 00:12:05,519
only one more thing that I need to show

272
00:12:04,120 --> 00:12:07,720
you before I let you loose on that

273
00:12:05,519 --> 00:12:09,760
assembly and that is a reminder about

274
00:12:07,720 --> 00:12:11,440
integer calling conventions and

275
00:12:09,760 --> 00:12:13,399
specifically it's said that you know

276
00:12:11,440 --> 00:12:15,320
there are eight argument registers a0

277
00:12:13,399 --> 00:12:17,959
through A7 the first two of which are

278
00:12:15,320 --> 00:12:19,880
used in return values so I'm telling you

279
00:12:17,959 --> 00:12:23,440
concretely that when we're returning a

280
00:12:19,880 --> 00:12:26,120
128bit value a z is going to be the

281
00:12:23,440 --> 00:12:29,440
least significant bits and a1 is going

282
00:12:26,120 --> 00:12:32,320
to be the most significant bits so

283
00:12:29,440 --> 00:12:34,600
with that now you have the information

284
00:12:32,320 --> 00:12:37,079
necessary in order to go and read this

285
00:12:34,600 --> 00:12:39,680
assembly and for this example I highly

286
00:12:37,079 --> 00:12:41,920
recommend you literally copy and paste

287
00:12:39,680 --> 00:12:45,079
the assembly put it into a text editor

288
00:12:41,920 --> 00:12:46,920
and make a comment on every single line

289
00:12:45,079 --> 00:12:49,320
so that you know what's going on and

290
00:12:46,920 --> 00:12:53,720
keep track of for each line do you think

291
00:12:49,320 --> 00:12:57,639
this is a a high a low B high bow treat

292
00:12:53,720 --> 00:12:59,959
the uh dissected as the uh dissected is

293
00:12:57,639 --> 00:13:02,720
the a high to start with delectable is

294
00:12:59,959 --> 00:13:06,079
the B sorry not a high dissected is the

295
00:13:02,720 --> 00:13:08,040
a to start with uh delectable is the B

296
00:13:06,079 --> 00:13:10,440
to start with and just keep track of a

297
00:13:08,040 --> 00:13:12,839
highs times a lows and so forth you will

298
00:13:10,440 --> 00:13:15,440
see that again it's upcast to 128 bits

299
00:13:12,839 --> 00:13:16,880
and the upper 128 bits is zero so that

300
00:13:15,440 --> 00:13:18,320
should give you you know a hint that

301
00:13:16,880 --> 00:13:20,680
there's going to be some pointless zero

302
00:13:18,320 --> 00:13:22,000
multiplication going on but still walk

303
00:13:20,680 --> 00:13:23,800
through every single assembly

304
00:13:22,000 --> 00:13:25,959
instruction write a comment for every

305
00:13:23,800 --> 00:13:29,480
line and you should see that our

306
00:13:25,959 --> 00:13:31,720
intuition behind this sort of oper ation

307
00:13:29,480 --> 00:13:34,120
going on in the code that the compiler

308
00:13:31,720 --> 00:13:36,880
generated is what's actually going on

309
00:13:34,120 --> 00:13:41,120
all right go off and use your debugger

310
00:13:36,880 --> 00:13:41,120
use some comments on the source lines

