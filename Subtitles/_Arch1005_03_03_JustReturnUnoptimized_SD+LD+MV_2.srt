1
00:00:00,760 --> 00:00:06,600
okay for this simple C source code

2
00:00:03,080 --> 00:00:08,960
compiled with no optimization we get

3
00:00:06,600 --> 00:00:11,000
this assembly and we talked about what

4
00:00:08,960 --> 00:00:12,719
each of these assembly instructions do

5
00:00:11,000 --> 00:00:15,240
but what I want to do now is walk

6
00:00:12,719 --> 00:00:18,640
through the code with you in slow motion

7
00:00:15,240 --> 00:00:20,920
as it were in my amazing slide wear

8
00:00:18,640 --> 00:00:23,560
debugger so we're going to cover this in

9
00:00:20,920 --> 00:00:25,119
slides one single step at a time before

10
00:00:23,560 --> 00:00:27,800
we go off and look at it in a real

11
00:00:25,119 --> 00:00:30,240
debugger GTP so in my slideware debugger

12
00:00:27,800 --> 00:00:32,239
we've got our registers up here and you

13
00:00:30,240 --> 00:00:34,239
can see that the initial values are all

14
00:00:32,239 --> 00:00:35,719
in green with this Cloverleaf symbol

15
00:00:34,239 --> 00:00:39,040
that's the way that I'm going to denote

16
00:00:35,719 --> 00:00:40,680
a starting value where the code imagine

17
00:00:39,040 --> 00:00:42,840
the debugger has stopped right here and

18
00:00:40,680 --> 00:00:45,399
it has not yet executed this first

19
00:00:42,840 --> 00:00:47,239
assembly instruction once we execute it

20
00:00:45,399 --> 00:00:48,960
we will show it in blue and we'll put a

21
00:00:47,239 --> 00:00:51,199
little X symbol but for now we're

22
00:00:48,960 --> 00:00:54,000
stopped we're about to execute this

23
00:00:51,199 --> 00:00:57,559
because the program counter is all fives

24
00:00:54,000 --> 00:00:59,480
628 saying that this all five 628 is the

25
00:00:57,559 --> 00:01:01,320
next assembly instruction execute but we

26
00:00:59,480 --> 00:01:02,840
have an executed it yet and therefore

27
00:01:01,320 --> 00:01:04,600
these are the current values of

28
00:01:02,840 --> 00:01:06,840
registers this is the current value of

29
00:01:04,600 --> 00:01:11,080
the stack the stack pointer has a value

30
00:01:06,840 --> 00:01:13,360
of 7 fs and 920 and there is a value

31
00:01:11,080 --> 00:01:15,960
remember the stack pointer points at the

32
00:01:13,360 --> 00:01:18,960
last place where there is used data on

33
00:01:15,960 --> 00:01:21,280
the stack but this data necessarily

34
00:01:18,960 --> 00:01:23,479
because we haven't executed any single

35
00:01:21,280 --> 00:01:25,799
instruction from main this data must

36
00:01:23,479 --> 00:01:29,079
have come from the function that call

37
00:01:25,799 --> 00:01:31,280
domain so we handwave over it but just

38
00:01:29,079 --> 00:01:33,799
in general there is a function that

39
00:01:31,280 --> 00:01:35,759
calls into main and when you return from

40
00:01:33,799 --> 00:01:38,000
main you return back to that function so

41
00:01:35,759 --> 00:01:39,840
we don't really care what this value is

42
00:01:38,000 --> 00:01:42,880
it's just some other data from some

43
00:01:39,840 --> 00:01:44,560
other code not our code not our concern

44
00:01:42,880 --> 00:01:46,520
and then I do want to remind you that

45
00:01:44,560 --> 00:01:48,479
anything below the stack pointer in

46
00:01:46,520 --> 00:01:50,640
terms of lower addresses remember high

47
00:01:48,479 --> 00:01:53,920
addresses high low addresses low so

48
00:01:50,640 --> 00:01:56,159
lower addresses are undefined so there

49
00:01:53,920 --> 00:01:58,240
is technically data there it is filled

50
00:01:56,159 --> 00:01:59,920
in with real values but we should just

51
00:01:58,240 --> 00:02:02,320
treat it and think of it as if it's

52
00:01:59,920 --> 00:02:04,280
undefined okay so let's get to Stepping

53
00:02:02,320 --> 00:02:06,439
we execute the first assembly

54
00:02:04,280 --> 00:02:10,560
instruction add I

55
00:02:06,439 --> 00:02:13,080
spsp -6 so it is an add instruction but

56
00:02:10,560 --> 00:02:15,480
the immediate in a add instruction the

57
00:02:13,080 --> 00:02:17,120
immediate 12 can have a positive or A

58
00:02:15,480 --> 00:02:20,920
negative value and that value is sin

59
00:02:17,120 --> 00:02:23,319
extended so this is adding -16 to the

60
00:02:20,920 --> 00:02:25,280
stack pointer so the net result of that

61
00:02:23,319 --> 00:02:28,160
is that the stack pointer will move down

62
00:02:25,280 --> 00:02:30,599
by hex 10 it was 920 it moves down to

63
00:02:28,160 --> 00:02:32,519
hex 10 and we denote that here with the

64
00:02:30,599 --> 00:02:35,120
red value showing that this assembly

65
00:02:32,519 --> 00:02:37,560
instruction which was just executed led

66
00:02:35,120 --> 00:02:40,159
to the following modified values so in

67
00:02:37,560 --> 00:02:42,519
red with a little M symbol here is

68
00:02:40,159 --> 00:02:44,560
indicating that only the stack pointer

69
00:02:42,519 --> 00:02:46,200
assembly instruction was modified here

70
00:02:44,560 --> 00:02:48,599
and of course the program counter will

71
00:02:46,200 --> 00:02:50,800
be modified every single time because

72
00:02:48,599 --> 00:02:52,519
every single step is going to change

73
00:02:50,800 --> 00:02:54,560
what the next assembly instruction that

74
00:02:52,519 --> 00:02:57,159
should execute is so after the stack

75
00:02:54,560 --> 00:02:59,000
pointer moves down these values are no

76
00:02:57,159 --> 00:03:01,120
longer undefined we're going to call

77
00:02:59,000 --> 00:03:03,680
them un initial ized so they are at

78
00:03:01,120 --> 00:03:06,400
higher addresses so they're above where

79
00:03:03,680 --> 00:03:08,920
the stack pointer points but we have not

80
00:03:06,400 --> 00:03:10,519
seen any code that actually initialize

81
00:03:08,920 --> 00:03:12,840
those values nothing has been written

82
00:03:10,519 --> 00:03:14,799
there yet so they're not undefined but

83
00:03:12,840 --> 00:03:17,080
they are uninitialized okay let's

84
00:03:14,799 --> 00:03:20,319
execute the next instruction this is a

85
00:03:17,080 --> 00:03:22,519
store double word store dword and stores

86
00:03:20,319 --> 00:03:25,640
are the exception to the rule that most

87
00:03:22,519 --> 00:03:29,040
things read from right to left stores

88
00:03:25,640 --> 00:03:31,280
read from left to right and the s0 is

89
00:03:29,040 --> 00:03:33,840
the thing that is being stored to memory

90
00:03:31,280 --> 00:03:35,280
we see parentheses and parentheses means

91
00:03:33,840 --> 00:03:37,599
that something's going to be used as a

92
00:03:35,280 --> 00:03:40,879
memory address and written there or read

93
00:03:37,599 --> 00:03:44,840
from memory so parentheses stack pointer

94
00:03:40,879 --> 00:03:47,959
so the stack pointer value is 7 FS 910

95
00:03:44,840 --> 00:03:51,560
so stack pointer plus 8 and this is

96
00:03:47,959 --> 00:03:54,720
basically just a Gano syntax or AT&T

97
00:03:51,560 --> 00:03:57,959
syntax sort of a hold over from the days

98
00:03:54,720 --> 00:03:59,640
where at uh AT&T Bell Labs invented C

99
00:03:57,959 --> 00:04:02,480
and other various things but anyway

100
00:03:59,640 --> 00:04:04,480
anyways parentheses inside element is

101
00:04:02,480 --> 00:04:06,239
the stack pointer outside element is the

102
00:04:04,480 --> 00:04:08,280
immediate that's going to be added to it

103
00:04:06,239 --> 00:04:10,920
and the net result is stack pointer plus

104
00:04:08,280 --> 00:04:14,239
8 go to memory at that address and store

105
00:04:10,920 --> 00:04:19,040
the value of s0 so stack pointer 7 FS

106
00:04:14,239 --> 00:04:22,280
910 plus 8 that brings us up to 7 FS 918

107
00:04:19,040 --> 00:04:24,240
and store the value from s0 which is

108
00:04:22,280 --> 00:04:28,639
also known as the frame pointer so store

109
00:04:24,240 --> 00:04:31,039
the value from s0 which is 7 FS aa8

110
00:04:28,639 --> 00:04:33,759
store that value to this memory address

111
00:04:31,039 --> 00:04:35,800
so whatever we calculated with the SP

112
00:04:33,759 --> 00:04:38,240
plus 8 treat that as memory and that is

113
00:04:35,800 --> 00:04:39,360
stack memory store the value from s0

114
00:04:38,240 --> 00:04:41,680
there so that has led to the

115
00:04:39,360 --> 00:04:43,639
modification of this memory location and

116
00:04:41,680 --> 00:04:45,600
the PC is moved again but no other

117
00:04:43,639 --> 00:04:47,479
changes no other register changes

118
00:04:45,600 --> 00:04:52,520
elsewhere through this code okay next

119
00:04:47,479 --> 00:04:55,280
instruction execute the ad I s0 sp16 so

120
00:04:52,520 --> 00:04:58,960
this is going to take stack pointer plus

121
00:04:55,280 --> 00:05:02,160
decimal 16 and put the result into the

122
00:04:58,960 --> 00:05:04,160
s0 register so we said s0 is our frame

123
00:05:02,160 --> 00:05:07,160
pointer so the frame pointer was

124
00:05:04,160 --> 00:05:09,919
pointing somewhere way up high the 7 fs

125
00:05:07,160 --> 00:05:11,639
aa8 and this was our saved copy of that

126
00:05:09,919 --> 00:05:13,639
but now as a consequence of this

127
00:05:11,639 --> 00:05:15,680
assembly instruction the frame pointer

128
00:05:13,639 --> 00:05:18,240
is going to be moved down and it will

129
00:05:15,680 --> 00:05:22,720
now now hold the value of Stack pointer

130
00:05:18,240 --> 00:05:27,160
which is 7fs 910 + 16 or hex 10 and so

131
00:05:22,720 --> 00:05:29,639
we get 7 FS 920 that is the new value

132
00:05:27,160 --> 00:05:31,919
for the frame pointer so the old frame

133
00:05:29,639 --> 00:05:34,880
pointer gets set here and the new frame

134
00:05:31,919 --> 00:05:38,000
pointer gets set sort of just outside of

135
00:05:34,880 --> 00:05:40,600
the frame for the main function okay now

136
00:05:38,000 --> 00:05:43,319
the next assembly instruction Li the LI

137
00:05:40,600 --> 00:05:45,680
is a it's actually ADDI but it is

138
00:05:43,319 --> 00:05:49,639
easier to read an Li just load the

139
00:05:45,680 --> 00:05:51,720
immediate 65 into a5 so this leads to

140
00:05:49,639 --> 00:05:55,639
the change of the a5 register and it

141
00:05:51,720 --> 00:05:58,440
gets the hex 41 the decimal 65 which is

142
00:05:55,639 --> 00:06:00,880
exactly what just return just returns it

143
00:05:58,440 --> 00:06:03,160
returns hex 41 so we have to see a hex

144
00:06:00,880 --> 00:06:04,800
41 at some point so that modifies the

145
00:06:03,160 --> 00:06:07,000
register and the program counter of

146
00:06:04,800 --> 00:06:11,080
course moves down once again the next

147
00:06:07,000 --> 00:06:13,000
instruction execute will be the 632 632

148
00:06:11,080 --> 00:06:16,880
okay the next assembly instruction to

149
00:06:13,000 --> 00:06:19,080
execute is the move from a5 into a z so

150
00:06:16,880 --> 00:06:22,000
the value inside the a5 register moves

151
00:06:19,080 --> 00:06:24,560
to the a z register so the hex 41

152
00:06:22,000 --> 00:06:27,319
getting moved into the a z register and

153
00:06:24,560 --> 00:06:30,000
why is that well that's because the ABI

154
00:06:27,319 --> 00:06:33,199
calling convention says that a0 is where

155
00:06:30,000 --> 00:06:35,800
you're going to return values out from

156
00:06:33,199 --> 00:06:38,680
functions so since just return just

157
00:06:35,800 --> 00:06:40,720
returns x41 before it Returns the a0

158
00:06:38,680 --> 00:06:42,479
should be holding hex 41 and that's

159
00:06:40,720 --> 00:06:44,160
exactly what the move accomplishes and

160
00:06:42,479 --> 00:06:46,800
since that's the only real thing that

161
00:06:44,160 --> 00:06:48,520
just return is doing we're basically now

162
00:06:46,800 --> 00:06:51,280
into the function epilogue we're going

163
00:06:48,520 --> 00:06:53,400
to start exiting out of this function so

164
00:06:51,280 --> 00:06:56,199
execute the next assembly instruction

165
00:06:53,400 --> 00:06:59,000
and what do we do we load a dword from

166
00:06:56,199 --> 00:07:01,560
memory at the address stack pointer plus

167
00:06:59,000 --> 00:07:03,199
h treat it as a memory address pull a

168
00:07:01,560 --> 00:07:05,680
d-word out of that memory address and

169
00:07:03,199 --> 00:07:10,479
put it into s0 so stack pointer still

170
00:07:05,680 --> 00:07:13,080
points here at 7 FS 910 add 8 and pull a

171
00:07:10,479 --> 00:07:16,400
value out of memory from that address 7

172
00:07:13,080 --> 00:07:20,800
fs aa8 and place that into the s0

173
00:07:16,400 --> 00:07:22,800
register so now s0 gets 7 fs aa8 and so

174
00:07:20,800 --> 00:07:24,919
functionally that means that the frame

175
00:07:22,800 --> 00:07:27,440
pointer goes way back up to wherever it

176
00:07:24,919 --> 00:07:32,520
was pointing when we came into main okay

177
00:07:27,440 --> 00:07:34,080
next instruction add I sp sp 16 so stack

178
00:07:32,520 --> 00:07:37,360
pointer points right here right now at

179
00:07:34,080 --> 00:07:39,280
7fs 910 but we're going to add 16 to

180
00:07:37,360 --> 00:07:43,199
that and place it back into the SP

181
00:07:39,280 --> 00:07:47,479
register so SP moves up by hex1 and we

182
00:07:43,199 --> 00:07:49,319
said that anything below SP is undefined

183
00:07:47,479 --> 00:07:50,879
so now we're going to say oh we don't

184
00:07:49,319 --> 00:07:52,560
know what those values are we never knew

185
00:07:50,879 --> 00:07:55,120
what those values were we're just going

186
00:07:52,560 --> 00:07:58,000
to be unconcerned now in reality they do

187
00:07:55,120 --> 00:08:02,680
totally still hold the actual literal

188
00:07:58,000 --> 00:08:05,000
value of 7 FFF aa8 and if you take the

189
00:08:02,680 --> 00:08:07,280
OST2 vulnerabilities class you will

190
00:08:05,000 --> 00:08:09,400
learn about how attackers abuse this

191
00:08:07,280 --> 00:08:12,599
fact in order to find bugs in

192
00:08:09,400 --> 00:08:15,120
uninitialized data access um code code

193
00:08:12,599 --> 00:08:16,759
that accesses data that it shouldn't uh

194
00:08:15,120 --> 00:08:19,639
that you know is supposed to be treated

195
00:08:16,759 --> 00:08:21,800
as uninitialized or undefined but for

196
00:08:19,639 --> 00:08:23,879
now focusing just on the RISC-V we

197
00:08:21,800 --> 00:08:25,599
know that stack pointer moved up and we

198
00:08:23,879 --> 00:08:27,800
are just about to exit out of the

199
00:08:25,599 --> 00:08:30,919
function return was actually a

200
00:08:27,800 --> 00:08:35,919
compressed J off R and compressed Jr was

201
00:08:30,919 --> 00:08:39,680
really a Jr and Jr was a jump register

202
00:08:35,919 --> 00:08:42,880
and jump register was actually a jwar a

203
00:08:39,680 --> 00:08:45,000
jump and link register but all the r is

204
00:08:42,880 --> 00:08:48,519
really doing is it's just setting the

205
00:08:45,000 --> 00:08:51,360
program counter to the ra the ra for

206
00:08:48,519 --> 00:08:53,320
return address register so ra had some

207
00:08:51,360 --> 00:08:55,160
initial value and it actually seems to

208
00:08:53,320 --> 00:08:57,360
still have that green with Clover Leaf

209
00:08:55,160 --> 00:08:59,839
so this never changed throughout the

210
00:08:57,360 --> 00:09:01,720
entire execution of the code and that's

211
00:08:59,839 --> 00:09:03,480
basically what we would expect just

212
00:09:01,720 --> 00:09:06,440
return doesn't call any other functions

213
00:09:03,480 --> 00:09:08,600
so whatever call domain calls into main

214
00:09:06,440 --> 00:09:11,240
and it set a return address so that it

215
00:09:08,600 --> 00:09:13,519
could main could return back out to it

216
00:09:11,240 --> 00:09:16,320
when it's done and therefore the return

217
00:09:13,519 --> 00:09:19,079
address just is used to return back to

218
00:09:16,320 --> 00:09:23,160
main so this R is really just setting

219
00:09:19,079 --> 00:09:25,120
the PC back to ra and therefore the PC

220
00:09:23,160 --> 00:09:27,720
is now going to change to that and it

221
00:09:25,120 --> 00:09:29,519
will pick up execution from there in the

222
00:09:27,720 --> 00:09:32,600
function that call domain so that was

223
00:09:29,519 --> 00:09:35,560
our slow motion debugging ins slides of

224
00:09:32,600 --> 00:09:39,360
just return. C we will look at it in GDB

225
00:09:35,560 --> 00:09:39,360
debugger in a coming video

